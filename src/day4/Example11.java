package day4;
/* Concept of constructor
 * -> Constructor cannot be considered as a method as it does not have a return type/void
 * -> Default constructor is created in the stack memory. 
 * -> JVM creates the default constructor (constructor name MUST be same as class name)
 * -> Once an object is created, global variables will be assigned to the constructor;
 * -> When a constructor is written, creating of an object is compulsory;
 * -> This means that heap memory will be used to store the object, affecting optimum performance 
 * */

// Constructor chaining
// -> this() keyword for constructors within the same class
// -> super() keyword to call constructor from the base class

class User {
	int number = 2;
	static int staticNumber;
	
	User() { // Default Constructor
		this(2, "ok");
		number = 10;
		System.out.println(number);
		
	} // End of constructor
	
	User (int number, String name) {
		this(true,55555);
		System.out.println(number + " "+name);
	}
	
	User (boolean data, long digit) {
		
		System.out.println(data + " "+digit);
	}
	
	
} // End of User
public class Example11 {

	public static void main(String[] args) {

		//new User(); // Calls the User() constructor
		//new User(2, "OK"); // Calls the User (int number, String name) constructor
		//new User(true, 5555); // Calls the User (boolean data, long digit) constructor
		new User();
		//new User(2, "OK", true, 5555);
		
		
		/* If you call the constructor using reference, you will get a compilation error. 
		 * However, you can use the reference to call a method
		 * For e.g.:
		 * User uRef = new User();
		 * uRef.User(10, "test");
		 */
	}

}
