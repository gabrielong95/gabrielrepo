package day4;

// Concept of Inheritance
// Java supports multilevel inheritance for class level
// Java supports multiple inheritance through interface

class Programming { // Base class/super class/parent class
	void showProgramming() {
		System.out.println("I am in Programming class..");
	}
} // End of Programming

class Java extends Programming { // Derived class/sub class/ child class
	void showJava() {
		System.out.println("I am in Java class..");
	}
} // End of Java

class Kotlin extends Java {
	void showKotlin() {
		System.out.println("I am in Kotlin class..");
	}
} // End of Kotlin

class KotlinSecurity extends Kotlin {
	void getDetails() {		
		showProgramming();
		showJava();
		showKotlin();
	} // End of getDetails()
	
} // End of KotlineSecurity

class MyClass extends KotlinSecurity {
	
	void getDetails() {
		System.out.println("data from MyClass......");
	}
	
	void getMyClass() {
		this.getDetails(); // Which getDetails are we going to call? Line 36 or line 26? (calls line 36)
		super.getDetails(); // Access Base class / Super class / Parent class
	} 
	
} // End of MyClass

public class Example13 {
	public static void main(String[] args) {
		new MyClass().getMyClass();
		
		//new KotlinSecurity().getDetails();
	}
}
