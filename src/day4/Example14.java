package day4;

// Concept of Method Overriding or Dynamic Polymorphism

class Laptop {
	
	void getDetails1() {
		System.out.println("I am in Laptop ==> getDetails1() ");
	}
	
	// If base class have static method, derived class cannot override.
	static void getDetails2() {
		System.out.println("I am in Laptop ==> getDetails2() ");
	}
	
} // End of Laptop

class OmenHP extends Laptop {
	
	// Override means inheriting a method from the base class. 
	// To ensure overriding
	// 1. Must be parent-child relationship; (inheritance)
	// 2. Ensure that method name must be same;
	// 3. Method-signature (parameter data type) must be same
	// Note: Return-type can be the same or different.
	@Override	
	void getDetails1() {
		System.out.println("I am in OmenHP ==> getDetails1() ");
	}
	
	//@Override - Static method CANNOT override
	static void getDetails2() {
		System.out.println("I am in OmenHP ==> getDetails2() ");
	}	
} // End of OmenHP


public class Example14 {

	public static void main(String[] args) {
		Laptop refLaptop = new OmenHP();
		refLaptop.getDetails1();
		refLaptop.getDetails2();
		
		// You cannot create an object of the base class that is derived from the child class.
		// Use type casting to solve this issue
		// OmenHP refOmenHP = new Laptop(); \\
		
		// Error: ClassCastException occurs when:
		// Creating an object of base/parent class and then referring to its child/derived class
		// And through the derived class, you call the method.
		/*
		 * OmenHP refOmenHP = (OmenHP) new Laptop();
		 * refOmenHP.getDetails1();
		 * refOmenHP.getDetails2();
		 */
		
	} // End of main

} // End of Example14
