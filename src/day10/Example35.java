package day10;

abstract class ProjectManagement {
	abstract void manage();
}

abstract class SixSigma extends ProjectManagement { }

abstract class Agile extends ProjectManagement { }

class BlackBelt extends SixSigma {

	@Override
	void manage() {
		System.out.println("Black Belt Certified...");
		
	} 
	
} // End of BlackBelt

class GreenBelt extends SixSigma {

	@Override
	void manage() {
		System.out.println("Green Belt Certified...");
	}
}

class Scrum extends Agile {

	@Override
	void manage() {
		System.out.println("Scrum Certified...");
	}
}
public class Example35 {

	public static void main(String[] args) {
		
		
		ProjectManagement refProjectManagement[] = new ProjectManagement[3];
		refProjectManagement[0] = new BlackBelt();
		refProjectManagement[1] = new GreenBelt();
		refProjectManagement[2] = new Scrum();
		
		// Length of refProjectManagement = 3
		for (int i = 0; i < refProjectManagement.length; i++) {
			if (refProjectManagement[i] instanceof GreenBelt) 
				refProjectManagement[i].manage();
		}
		
		SixSigma refSixSigma[] = new SixSigma[2];
		refSixSigma[0] = new BlackBelt();
		refSixSigma[1] = new GreenBelt();
		// refSixSigma[2] = new Scrum();
		
		for (int i = 0; i < refProjectManagement.length; i++) {
			
			// instanceof operator checks that GreenBelt belongs to SixSigma;
			// or if Scrum belongs to Agile
			if (refSixSigma[i] instanceof GreenBelt)  // Generics
				refSixSigma[i].manage();
			
		}
		
	}

}
