package day10;
// From Example36
public class Person {

	// To do task for 21/4/21
	// int hash code ??
	// boolean equals??
	
	String personName;
	public Person(String personName) {
		this.personName = personName;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((personName == null) ? 0 : personName.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Person other = (Person) obj;
		if (personName == null) {
			if (other.personName != null)
				return false;
		} else if (!personName.equals(other.personName))
			return false;
		return true;
	}
	
	/*
	 * 
	 * hashCode() checks memory address
	 * equals() checks the value
	 * 
	 */

	  
		  
}
