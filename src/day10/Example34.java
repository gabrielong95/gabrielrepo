package day10;

class OuterA {

	void methodOfOuterA() { // Method of local inner class
		
		int number = 10;
		
		class InnerB {	
			int number = 30;
			void methodOfInnerB() {
				System.out.println("I am in innerB...");
				System.out.println(number);
			} // End of methodOfInnerB()
			
		} // End of InnerB class
		
		InnerB refInnerB = new InnerB();
		refInnerB.methodOfInnerB();
		
	} // End of methodOfOuterA()
	
} // End of OuterA class

public class Example34 {

	public static void main(String[] args) {
		OuterA refOuterA = new OuterA();
		refOuterA.methodOfOuterA();
	}

}
