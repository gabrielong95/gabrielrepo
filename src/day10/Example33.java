package day10;

import java.util.Scanner;

//import day10.OuterClassDemo.InnerClassDemo;

class OuterClassDemo {
	
	public void innerClassMethod() {
		refInnerClassDemo.getDetailsInnerCLassDemo();
	}
	
	private class InnerClassDemo { 
		// An inner class can also be called a nested class; used for security purposes
			
		
		void getDetailsInnerCLassDemo() {
			System.out.println("Enter your password: ");
			Scanner refScanner = new Scanner (System.in);
			String password = refScanner.next();
			System.out.println(password);
		}
		
	} // End of InnerClassDemo
	
	InnerClassDemo refInnerClassDemo = new InnerClassDemo(); // (Approach 1)
	// Can create an object to call access it

	
	
} // End of OuterClassDemo

public class Example33 {

	public static void main(String[] args) {
		
		// How to access inner class (for not private: Approach 1 & 2):
		 
		// ------------------------- APPROACH 1 ------------------------- \\
		OuterClassDemo refOuterClassDemo= new OuterClassDemo();
		//refOuterClassDemo.refInnerClassDemo.getDetailsInnerCLassDemo();
		
		// ------------------------- APPROACH 2 ------------------------- \\
		//OuterClassDemo.InnerClassDemo refInnerClassDemo= refOuterClassDemo.new InnerClassDemo();
		//refInnerClassDemo.getDetailsInnerCLassDemo();
		
		
		// ------------------------- APPROACH 3 ------------------------- \\
		// If inner class is private, encapsulate the method and create the reference in OuterClass
		refOuterClassDemo.innerClassMethod();
	}

}
