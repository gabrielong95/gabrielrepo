package day10;

// String class

public class Example36 {
	
	public static void main(String[] args) {
		/*
		 * Note: If comparing class (object or String), we use the .equals() method to compare.
		 * 
		 * hashCode() checks memory address
		 * equals() checks the value
		 * We only use the == operator to compare primitive data types such as int, double, etc. 
		 */
//		String ref1 = "data1";
//		String ref2 = "data1";
//		String ref6 = "data2";
//		String ref3 = new String("data1");
//		String ref4 = new String("data1");
//		String ref5 = new String("data1");
		
		Person refPerson1 = new Person("data1");
		Person refPerson2 = new Person("data1");
		
		System.out.println(refPerson1.hashCode());
		System.out.println(refPerson2.hashCode());
		
		/*
		 * System.out.println(ref1.hashCode() == ref2.hashCode());
		 * System.out.println(refPerson1.hashCode() == refPerson2.hashCode());
		 * System.out.println(refPerson2 == refPerson1);
		 * System.out.println(refPerson2.equals(refPerson1));
		 */
		
		/*
		// To check if ref1 and ref2 is the same using == operator
		if (ref1 == ref2) { 
			System.out.println("same");
		} else {
			System.out.println("not same");
		}
		
		// To check if ref1 and ref2 is same using .equals() method
		if (ref1.equals(ref2)) {
			System.out.println("same");
		} else {
			System.out.println("not same");
		}
		
		// To check if ref2 and ref3 is the same using == operator
		if (ref2 == ref3) {
			System.out.println("same");
		} else {
			System.out.println("not same");
		}
		
		// To check if ref2 and ref3 is same using .equals() method
		if (ref2.equals(ref3)) {
			System.out.println("same");
		} else {
			System.out.println("not same");
		}
		*/
		
		
		
		// To check if ref3 and ref4 is the same using == operator
//		if (ref3 == ref4) { // Both are pointing to 2 different memory addresses
//			System.out.println("same");
//		} else {
//			System.out.println("not same"); // Hence it is not same.
//		}
//
//		// To check if ref3 and ref4 is same using .equals() method
//		if (ref3.equals(ref4)) { // Both are pointing to same memory address in String Constant Pool in the Heap memory
//			System.out.println("same"); // Hence it is the same.
//		} else {
//			System.out.println("not same");
//		}		
//		
//		// To check if refPerson1 and refPerson2 is same using == operator
//		if (refPerson1.equals(refPerson2)) {
//			System.out.println("refPerson1 is same as refPerson2");
//		} else {
//			System.out.println("refPerson1 is not same as refPerson2");
//		}
		
	}

}
