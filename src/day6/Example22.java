package day6;

// Example of a functional interface -> Restricts creating of one more abstract method
// Functional interface can have only ONE abstract method

/*
 * @FunctionalInterface
 * interface One {
 *  void displayMethod1();
 *	void displayMethod2();
 * }
 */

abstract class OptimumCar1 {
	final static int number = 10;
	void display1() {
		System.out.println("Printing from display1()");
	}
	abstract void display2();
}

/* interface class always starts with abstract; it is a fully abstract class.
 * However we don't write abstract, as the compiler understands that interface class 
 * is part of abstract.
 */

interface OptimumCar2 {
	/* You cannot write a normal concrete method in interface class.
	 * If you want to write a normal concrete method, put it as a default method. e.g. default void display() { ... }
	 * Otherwise there will be a compilation error
	 */
	//void display3() {
		// Method with body
	//} 
	
	int number = 10; // It is final static by default
	
	default void display4() { // You can call private/private static methods here
		display5(); 
		display6();
	}	
	static void display5() {
		System.out.println("Printing from display5()");
	}
	private static void display6() {
		System.out.println("Printing from display6()");
	}
	abstract void display8();
} // End of OptimumCar2 class



class MyClass extends OptimumCar1 implements OptimumCar2{

	
	void display9() {
		display4();
	}
	
	@Override
	public void display8() {
		System.out.println("Printing from display8()");
		
	}

	@Override
	void display2() {
		System.out.println("Printing from display2()");
	}
/* Question: Can we extends more than one abstract class? Answer: No. Only can extends one abstract class.
 * 
 * Question: Can we implements more than one interface? Answer: Yes e.g.:
 * class MyClass extends OptimumCar1 implements OptimumCar2, SecondClass, ThirdClass
 * 
 * Interface is more preferred because we can extends n number of interfaces
 */
	
}

public class Example22 {
	public static void main(String[] args) {
		MyClass refClass = new MyClass();
		refClass.display9();
		refClass.display8();
		refClass.display2();
	}
}
