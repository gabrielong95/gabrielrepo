package day6;

// Note: If an abstract class A extends to another abstract class B, then
// there is no need to override the abstract methods. I.e., abstract class A
// can be left empty.
abstract class Vehicle {
	abstract void withDriver();
	abstract void driverless();
} // End of Vehicle class

abstract class Car extends Vehicle {
	abstract void supportsAI();
}


abstract class MyCar extends Car {

} 


class Car1 extends MyCar {
	@Override
	void supportsAI() {
		System.out.println("inside supportsAI.....");		
	}

	@Override
	void withDriver() {
		System.out.println("inside withDriver.....");
	
	}

	@Override
	void driverless() {
		System.out.println("inside driverless.....");
		
	}
	
}


public class Example19 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
