package day6;

class Base {
	int number = 100;
	// Local variable cannot be static)
	Base(int number) { // Default constructor
		System.out.println(number); // 100 or 10 -> Prints out 10
		// To print 100, -> System.out.println(this.number);
	}
	
	void display (int num) {
		
	}
	
} // End of Base class


class Derived extends Base {
	Derived (int number) {
		super (number);
		System.out.println(number);
	}
} // End of Derived class


class SubDerived extends Derived {
	SubDerived(int number, String data) {
		super (number); // Pass the value to the parent, use super
		System.out.println(number + " "+data);
	}
}

public class Example21 {

	public static void main(String[] args) {
		new SubDerived(10, "data-1");

	}

}
