package day6;

// A constructor can be created within an abstract class.
abstract class Airline {
	int flightID;
	String flightName;
	
	Airline () {
		flightID = 100;
		flightName = "British Airbus";
	} // End of Airline constructor
	
	abstract void getFlightDetails() ;
}

class FlightDetails extends Airline {

	int flightID = 101;
	String flightName = "SIA";
	@Override
	void getFlightDetails() {
		System.out.println(super.flightID + " " +super.flightName);
		System.out.println(super.flightID + " " + this.flightID + " " +this.flightName);
	}
	
}

public class Example20  {
	public static void main(String[] args) {
		FlightDetails ref = new FlightDetails();
		ref.getFlightDetails();
	}
}
