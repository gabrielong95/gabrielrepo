package day16;

// Use Case: Suppose we have multiple threads running in the application
// We need to execute (run) any particular thread on a high priority, then
// how to solve it.

class ThreadDemo extends Thread {
	
	@Override
	public void run() {
		for (int i = 0; i <= 3; i++) {
			try {
				Thread.sleep(5000); // 5000ms = 5 sec
				
			} catch (Exception e) {
				System.out.println("Exception Handled..");
			}
			System.out.println(i);
			
			// Under main() thread, get the thread (sub process).
			System.out.println(currentThread().getName());
		}
	}
}

public class Example57 {

	public static void main(String[] args) { // main() thread
		ThreadDemo refThreadDemo = new ThreadDemo();
		Thread refThread1 = new Thread(refThreadDemo);
		Thread refThread2 = new Thread(refThreadDemo);
		Thread refThread3 = new Thread(refThreadDemo);
		Thread refThread4 = new Thread(refThreadDemo);
		Thread refThread5 = new Thread(refThreadDemo);
		
		refThread1.setName("Thread 1");
		refThread2.setName("Thread 2");
		refThread3.setName("Thread 3");
		refThread4.setName("Thread 4");
		refThread5.setName("Thread 5");
		
		refThread1.start();
		refThread2.start();
		
		try {
			refThread2.join();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		
		refThread3.start();
		refThread4.start();
		refThread5.start();
		
	}

}
