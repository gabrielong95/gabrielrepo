package day7;

public class Department {
	private String departmentName;
	private int departmentID;

	Student refStudent; // This line of code means Department class has a (has-a) relationship with Student class.
	Professor refProfessor;

	Department (Student refStudent, Professor refProfessor, String departmentName, int departmentID) {
		this.refStudent = refStudent;
		this.refProfessor = refProfessor;
		this.departmentName = departmentName;
		this.departmentID = departmentID;
	}
	
	@Override
	public String toString() {
		return (refStudent + "\n\n" +refProfessor + "\n\nDepartment Name: "+departmentName + "\nDepartment ID: "+departmentID);	
		
	}
}
