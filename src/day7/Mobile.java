package day7;

// 
public class Mobile {
	WhatsApp refWhatsApp; // This line of code means Mobile has a (has-a) relationship with WhatsApp class.

	public WhatsApp getRefWhatsApp() {
		return refWhatsApp;
	}

	public void setRefWhatsApp(WhatsApp refWhatsApp) {
		this.refWhatsApp = refWhatsApp;
	}
}
