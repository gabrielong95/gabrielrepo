package day7;

public class University {
	
	Department refDepartment; // This line of code means University class has a (has-a) relationship with Department class.
	int universityID;
	
	University (Department refDepartment, int universityID) {
		this.refDepartment = refDepartment;
		this.universityID = universityID;
	}
	
	@Override
	public String toString() {
		return "\n"+refDepartment.toString() + "\n\nUniversity ID: "+universityID;
	}
}
