package day7;

final class UserAccessPermission {
	
}


// You cannot extend to a final class, there is a restriction.
// i.e. If your base class is final, you can't access.
// Final class is used for security purposes

/*
 * class SubFinalDemo extends UserAccessPermission {
 *	
 * }
 */

/*
 * interface FinalDemo1 {
 *	
 * }
 */

class Mobile1 {
	
	// A final method cannot be override
	final void application() {
		
	}
	
}

class SubMobile extends Mobile1 {
	
	/*
	 * @Override
	 * void application() {
	 * 	
	 * }
	 */
}

class Laptop {
	final int number = 20; // Final variable cannot be modified!
	
	// private, static, public, default = Access modifiers
	// final = Access specifier
	
	// final in java refers to constant
	
	void getNumber(int value) {
		// number = value;	--> This will result in compilation error 
	}
}


public class Example23 {
	
	public static void main(String[] args) {
		

	}

}
