package day7;

public class WhatsApp {
	
	Chat refChat; // This line of code means WhatsApp has a (has-a) relationship with Chat class.

	public Chat getRefChat() {
		return refChat ;
	}

	public void setRefChat(Chat refChat) {
		this.refChat = refChat;
	}
}
