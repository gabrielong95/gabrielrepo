package day7;

public class Professor {
	private String professorName;
	private int professorID;
	
	Professor (String professorName, int professorID) {
		this.professorName = professorName;
		this.professorID = professorID;
	}
	
	@Override
	public String toString() {
		return ("Professor Name: "+professorName + "\nProfessor ID: "+professorID);
	}
}
