package day7;

public class Student {
	private String studentName;
	private int studentID;
	
	Student (String studentName, int studentID) {
		this.studentName = studentName;
		this.studentID = studentID;
	}
	
	@Override // toString() method returns the memory address.
	public String toString() {
		return ("Student Name: "+studentName + "\nStudent ID: "+studentID);
	}
	
}
