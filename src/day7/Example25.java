package day7;

public class Example25 {

	public static void main(String[] args) {
		
		Student refStudent = new Student("Student 1", 000);
		Professor refProfessor = new Professor ("Professor 1", 999);
		Department refDepartment = new Department (refStudent, refProfessor, "English", 222);
		University refUniversity = new University(refDepartment, 702);
		
		
		Student refStudent2 = new Student("Student 2", 001);
		Professor refProfessor2 = new Professor("Professor 2", 998);
		Department refDepartment2 = new Department(refStudent2, refProfessor2, "Maths", 333);
		University refUniversity2 = new University(refDepartment2, 701);
		
		System.out.println(refUniversity);
		System.out.println("\n--------------------------------");
		System.out.println(refUniversity2);
		
	}

}
