package day7;

public class Example24 {

	public static void main(String[] args) {
		
		Chat refChat = new Chat();
		refChat.setChatMessage("HELLO");
		
		WhatsApp refWhatsApp = new WhatsApp();
		refWhatsApp.setRefChat(refChat);
		
		Mobile refMobile = new Mobile();
		refMobile.setRefWhatsApp(refWhatsApp);

		System.out.println(refMobile.getRefWhatsApp().getRefChat().getChatMessage());
	}

}
