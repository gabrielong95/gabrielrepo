package pack1;

import pack2.User;


class Testing{
	
	/*
	 * public void publicMethod() { toAccess(); }
	 * 
	 * protected void toAccess() { System.out.println("Hi"); }
	 */
}

public class JavaTest1Question1 {

	public void publicMethod(String name) {
		
		toAccess(name);
	}
	
	protected static void toAccess(String name) {
		System.out.println(name);
	}
	
	public static void main(String[] args) {
		/*
		 * javac compiles a .java file into a .class file
		 * Java accepts the name of the class as a parameter
		 */
		JavaTest1Question1.toAccess("Hello");
		
		
		String ref1 = args[0];
		String ref2 = args[1];
		String ref3 = args[2];
		
		
		//System.out.println("Arg is " + ref3);
		
		
		/*
		 * User userRef = new User(); userRef.setUserName("James");
		 * System.out.println(userRef.getUserName());
		 */
	}
	
	

}
