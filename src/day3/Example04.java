package day3;

class Customer {
	static int customerID = 100;
	String customerName = "James";
	
	void getDetails1() {
		System.out.println(customerID);
		System.out.println(customerName);
		
	} // end of getDetails1()
	
	static void getDetails2() {
		
		System.out.println(customerID);
		 
	} // end of getDetails2()
} // end of Customer class

public class Example04 {
	public static void main (String[] args) {
		
		// Call getDetails1() //non-static method
		Customer refCustomer = new Customer();
		refCustomer.getDetails1();
		
		// Call getDetails2()
		Customer.getDetails2();
		// OR 
		refCustomer.getDetails2();
		
		//Customer c = new Customer();
		//System.out.println(c);
	}

} // end of Example04
