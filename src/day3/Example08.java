package day3;
// Another concept/demonstration of method overloading

class Mobile {
	private void getFeatures(boolean wifi) { // True or false
		System.out.println(wifi);
	}
	
	public void getFeature (boolean value) {
		getFeatures(value);
	}
	
	public String getFeatures(String brandName) {
		return brandName;
	}
	
	// This method has to be a String because String can hold both integers and String, in this case.
	String getFeatures(int imei, String mobileLocation) {
		return imei + " "+mobileLocation;
	}
	
	void getFeatures(long mobileDiscount, float mobilePrice) {
		System.out.println(mobileDiscount + " "+mobilePrice);
	}
	
} // end of Mobile class


public class Example08 {

	public static void main(String[] args) {
		Mobile refMobile = new Mobile();
		//System.out.println(refMobile.getFeature(false));
		System.out.println(refMobile.getFeatures("nokia"));
		System.out.println(refMobile.getFeatures(1234567890, "area 51"));
		refMobile.getFeatures(1L, 89F);
	}

}
