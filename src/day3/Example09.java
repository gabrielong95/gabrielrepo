package day3;

class UserInformation {
	void getUserDetails(Object data) { // Get parameters
		System.out.println(data); // Print the value
	} // end of getUserDetails()
} // end of UserInformation class

class Person {
	String personName = "Gosling";
	@Override
	public String toString() {
		return personName;
	}
	
} // end of Person class

public class Example09 {
	public static void main(String[] args) {
		int userID = 100;
		String userName = "James";
		float userCode = 1234.543f;
		
		// Call getUserDetails() and pass the values
		UserInformation refUserInfo = new UserInformation();
		refUserInfo.getUserDetails(userID);
		refUserInfo.getUserDetails(userName);
		refUserInfo.getUserDetails(userCode);
	
		Person refPerson = new Person();
		refUserInfo.getUserDetails(refPerson);
		
		// This works too
		refUserInfo.getUserDetails(refPerson.personName);
	} // end of main method

} // end of Example09
