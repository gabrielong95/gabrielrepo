package day3;

// Encapsulation Demo

public class Example06 {

	public static void main(String[] args) {
		University refUniversity = new University();
		refUniversity.setUniversityID(100);
		refUniversity.setUniversityName("Name");
		refUniversity.setUniversityLocation("Location");
		
		System.out.println(refUniversity.getUniversityID() + " " +refUniversity.getUniversityName() + " " +refUniversity.getUniversityLocation());
	}

}
