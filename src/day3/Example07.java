package day3;

// Method overloading example

public class Example07 {
	
	public static void main (String[] args) {
		main("hello from line 8");
	} // end of main()
	
	
	public static void main (String data) {
		System.out.println(data);
		main(10);
	} // end of main()
	
	
	public static void main (int number) {
		System.out.println(number);
		System.out.println(main("hello from line 20", 500));
	} // end of main()

	
	public static String main (String data, int number) {
		return data + " " +number;
		
	} // end of main()
	
} // end of Example07 class
