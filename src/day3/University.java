package day3;
// No main method inside encapsulated class!
public class University {
	
	private int universityID;
	private String universityName;
	private String universityLocation;
	
	public int getUniversityID() {
		return universityID;
	}
	public void setUniversityID(int universityID) {
		this.universityID = universityID;
	}
	public String getUniversityName() {
		return universityName;
	}
	public void setUniversityName(String universityName) {
		this.universityName = universityName;
	}
	public String getUniversityLocation() {
		return universityLocation;
	}
	public void setUniversityLocation(String universityLocation) {
		this.universityLocation = universityLocation;
	}
	
	
	
}
