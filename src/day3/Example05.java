package day3;

// Concept of local and global variable

class User {
	
	int userID = 1000; // Global variable 
	String userPassword = "user123"; // Global variable
	
	void getDetails(int userID, String password) { // Local variable --> 50 and newuser123
		
		// Assign the local variable to global variable
		// In this case, this.userID is the global variable
		// Similarly, userPassword is the global variable, while password is the local variable
		
		// If global and local variables are the same, then use the "this" keyword
		// If not the same name, then don't need to use "this" keyword
		this.userID = userID;
		userPassword = password;
	
	} // end of getDetails()
	
	void showDetails() {
		System.out.println(userID + " "+userPassword); // We are calling the global variable
	} // end of showDetails()
	
} // end of User class


public class Example05 {
	
	public static void main(String[] args) {
		User refUser = new User();
		refUser.getDetails(50, "newuser123");
		refUser.showDetails();
	}

}
