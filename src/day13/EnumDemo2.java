// Example45
package day13;

import day13.MyApplication.LoanInsurance;

//Interface can only contain non-concrete (empty) method.
interface Loan {
	void homeLoanPlan();
	void carLoanPlan();
} // end of Loan

interface Insurance {
	void individualPlan();
	void corporatePlan();
} // end of Insurance



class MyApplication {
	
	// If you implement an interface, you have to @Override all methods from the interface that you implements
	// But you can feel free to add your own methods inside.
	enum LoanInsurance implements Loan, Insurance{
		
		PLAN_A;

		void newMethod(){
			System.out.println("Testing..");
		}
		
		@Override
		public void individualPlan() {
			System.out.println("individualPlan");
		}

		@Override
		public void corporatePlan() {
			System.out.println("corporatePlan");
			
		}

		@Override
		public void homeLoanPlan() {
			System.out.println("homePlan");
			
		}

		@Override
		public void carLoanPlan() {
			System.out.println("carPlan");
		}
		
	} // end of enum LoanInsurence
	
	void getEnumData() {
		LoanInsurance.PLAN_A.homeLoanPlan();
		LoanInsurance.PLAN_A.individualPlan();
		LoanInsurance.PLAN_A.newMethod();
	}
} // End of MyApplication


public class EnumDemo2 {

	public static void main(String[] args) {
		MyApplication refMyApplication = new MyApplication();
		refMyApplication.getEnumData();

	}

}
