package day13;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;

// Generics 

// What is Generics ==> Compile type type safety


// We create an interface called "Language" of type <T>.
interface Language<T>{			// T means Type
	
	// abstract method
	void programming(T refT);   // T means Type, refT means reference of type T
}

// 
class Java<OOP> implements Language<OOP>{

	// Override the abstract method from the implemented interface
	@Override
	public void programming(OOP refT) {			// OOP is a Type, refT is a reference of the Type
		
		System.out.println(refT);	
	}
}

public class GenericsDemo {

	public static void main(String[] args) {
		int number1 = 100;
		
		Integer number2 = 200;
		Double number3 = 0.004;

		
		Java<Double> refJavaIngeter = new Java<Double>();
		//Java<int> refJavaInteger = new Java<int>();			error	
		refJavaIngeter.programming(number3);

		Java<String> refJavaString1 = new Java<String>();
		refJavaString1.programming("Hello");
		String value = "data";
		
		Java<String> refJavaString = new Java<String>();
		refJavaString.programming(value);

		Employee refEmployee = new Employee();
		refEmployee.setEmployeeName("James");
		refEmployee.setEmployeeID(1);
		
		Java<Employee> refJavaEmployee = new Java<Employee>();
		refJavaEmployee.programming(refEmployee);

	}

}

class Employee{
	
	private String employeeName;
	private int employeeID;
	
	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}
	
	public void setEmployeeID(int employeeID) {
		this.employeeID = employeeID;
	}
	
	@Override
	public String toString() {
		return employeeName + " "+employeeID;
	}
	
}