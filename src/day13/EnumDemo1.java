// Example44
package day13;

//First of all, don't get confused with the term called ==> enumeration

//In Java, we have Enumeration interface (collection framework) ==> java.util.Enumeration
//enumeration() method is used ==> java.util.Collections.enumeration()

//Then what is enum??
//how to define it??

//enum is a reserved java keyword (Java 1.5)
//Enum (abstract class) ==> java.lang.Enum (Oracle documentation)
//JVM internally works for values(), ordinal(), valuesOf()
//purpose of enum ==> compile time type safety

import day13.Payment.PaymentOptions;

class Payment {
	
	// enum is a special class in Java (cannot be changed for a long time; i.e. Gender)
	// If it's something that is constantly changing, don't use enum.
	enum PaymentOptions {
		// Constant Reference
		VISA("Connect to Server 1"),
		PAYPAL("Connect to Server 2"),
		MASTERCARD("Connect to Server 3"),
		PAYWAVE("Connect to Server 4");
		
		private String serverConnection;
		
		PaymentOptions(String serverConnection) { // Constructor of enum which takes in String serverConnection in this example
			this.serverConnection = serverConnection;
		}
		
		
		public String getConnection() {
			return serverConnection;
		}
		
	} // end of enum PaymentOptions
	
	
	// get the data from enum
	void getEnumData() {
		
		// .ordinal() prints out the index of where the variable is, according to how it is declared in enum
		// index starts from 0, same as Array.
		for (PaymentOptions temp : PaymentOptions.values()) {
			System.out.println(temp.ordinal() + " "+temp.name() + " "+temp.getConnection());
		}
		
		// enum to String (constant reference ==> String)
		String connection1 = PaymentOptions.VISA.getConnection();			// approach 1
		System.out.println(connection1);
		
		// To get enum constant by name, valuesOf()
		PaymentOptions connection2 = PaymentOptions.valueOf("PAYPAL");		// approach 2
		System.out.println(connection2.getConnection());
		
	} // end of getEnumData()
			
} // end of Payment class 



public class EnumDemo1 {

	public static void main(String[] args) {
		Payment refPayment = new Payment();
		refPayment.getEnumData();  // calling line 44

	}

}
