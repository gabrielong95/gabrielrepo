// Example47
package day13;

import static java.lang.Math.sqrt;
import static java.lang.System.out;

public class StaticImportDemo {

	public static void main(String[] args) {
		
		// static imports will import all static data so that can use without class a class data
				out.println("Hello using static import..");

				System.out.println(sqrt(10));
		

	}

}

//Do it from your side

//you can create two packages name as package1 				package2
							//		   --------					--------
							//			Test1		 			Test2
							//			 static method1()		  call the method1() by using static import

