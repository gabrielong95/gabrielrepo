package day5bank;

public class BusinessLogic {
	public static CentralBank getDetails(String choice) {
		
		if (choice.equals("bank1")) {
			return new Bank1();
		} else if (choice == "bank2") {
			return new Bank2();
		} else if (choice == "nob") {
			return new NotABank();
		}
		
		return null;
	}
}
