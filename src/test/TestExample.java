package test;

import java.math.BigDecimal;

import pack1.JavaTest1Question1;

public class TestExample extends JavaTest1Question1 {    
    public static void main(String[] args)   
    {   
    	// Is there a way to access protected method from another package without static keyword?
    	JavaTest1Question1 refJava = new JavaTest1Question1();
    	refJava.publicMethod("Hello");
    	
    	JavaTest1Question1.toAccess("static");
    	
    	 
		/*
		 * // Create BigDecimal object BigDecimal bdValue1, bdValue2; // Create int
		 * variable int integerValue1, integerValue2;
		 * 
		 * // Assigning value into BigDecimal object bdValue1 = new
		 * BigDecimal("-36755.99999"); bdValue2 = new BigDecimal("63745");
		 * 
		 * // It returns int value of BigDecimal integerValue1 = bdValue1.intValue();
		 * integerValue2=bdValue2.intValueExact();
		 * 
		 * // Displaying int Value System.out.println(
		 * "Returned int value is = "+integerValue1); // Displaying exact Integer Value
		 * System.out.println("Returned Exact Integer Value of " +bdValue2 + " is = " +
		 * integerValue2);
		 */
    	
    	
		/*
		 * BigDecimal bigDecimal1 = new BigDecimal("2.5678935743968547"); BigDecimal
		 * bigDecimal2 = new BigDecimal("1.2345678423438294"); BigDecimal bigDecimal3 =
		 * bigDecimal1.add(bigDecimal2); System.out.println("Big Decimal: "
		 * +bigDecimal3);
		 * 
		 * 
		 * 
		 * float float1 = 2.5678935743968547f; float float2 = 1.2345678423438294f; float
		 * float3 = float1 + float2; System.out.println("Float: " +float3);
		 * 
		 * double double1 = 2.5678935743968547; double double2 = 1.2345678423438294;
		 * double double3 = double1 + double2; System.out.println("Double: " +double3);
		 */

    	
    	
        
        }   
    }  

