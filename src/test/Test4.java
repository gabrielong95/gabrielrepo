package test;

public class Test4 {

	public static void main(String[] args) {
		String str1 = "data1";
		String str2 = "data1";
		String str3 = "data2";

		String str4 = "data1";
		
		String refString1 = new String("data1");
		String refString2 = new String("data1");
		
		// == operator checks if the objects point to the same memory location
		if (str1 == str2) {
			System.out.println("same");
		} else {
			System.out.println("not same");
		}
		
		// .equals() method compares the values of the object
		if (str1.equals(str2)) {
			System.out.println("same");
		} else  {
			System.out.println("not same");
		}
		
		

		
		if (refString1 == refString2) {
			System.out.println("same");
		} else {
			System.out.println("not same");
		}
		
		if (refString1.equals(refString2)) {
			System.out.println("same");
		} else {
			System.out.println("not same");
		}
		
		/*
		if (str1 == refString1) {
			System.out.println("same");
		} else {
			System.out.println("not same");
		}
		*/
	}

}
