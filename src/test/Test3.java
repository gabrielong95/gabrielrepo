// Day 2 - Example03
package test;

public class Test3 {

	public static void main(String[] args) {
		
		Test3Bank refBank = new Test3Bank();
		// Creating a reference called refBank for new object of Test3Bank()
		
		refBank.setBankID(123);
		refBank.setBankName("DBS");
		// Using the reference (of Test3Bank class) to call the setters of 
		// the Test3Bank() object, which passes some value as parameters.
		
		System.out.println(refBank.getBankID());
		System.out.println(refBank.getBankName());
		// Using the reference (of Test3Bank class) to call the getters of 
		// the Test3Bank() object, which returns some value(s)
	}
}
