// Day 2 - Example02
package test;

class Employee { // Name of class (Employee)
	static int employeeID = 12345; // static variable
	String employeeName = "Non-static employee name"; // non-static variable
	
	static void printEmployeeDetails1() {
		System.out.println(employeeID);
		
		// System.out.println(employeeName); --> Returns compilation error. 
		// Non-static variable (String employeeName = ".....";) cannot be 
		// accessed from a  static method.
	}
	
	void printEmployeeDetails2() {
		System.out.println(employeeID);
		System.out.println(employeeName);
		
		// As compared to the static method above, a non-static method
		// allows both static and non-static variables to be accessed.
		
		// ***** However, in order to access a non-static method, an 
		// object of the class has to be created! *****
		
	}
} // End of Employee class


public class Test2 {
	static String data1 = "ABC";
	static String data2 = "XYZ";
	String data3 = "non-static";
	
	
	public static void main(String[] args) {
		/*
		 * What does static and non-static mean?
		 * ******************** MEANING OF STATIC ********************
		 * The keyword static in Java means you don't have to create an 
		 * object(s). If the variable is static, the developer does not 
		 * have to create an object of the class. Static variable(s)/method(s) 
		 * can be directly accessed by providing the class name.
		 * For e.g.: ClassName.variablename or ClassName.methodname()
		 */

		/*
		 * ****************** MEANING OF NON-STATIC ******************
		 * In order to access non-static variable(s) or method(s), an
		 * object of the class has to be created. 
		 */
		
		System.out.println(data1);
		System.out.println(data2);
		
		// System.out.println(data3); --> Returns compilation error, static method
		// cannot access non-static variable!
		
		Employee.printEmployeeDetails1(); 
		// Static method of printEmployeeDetails() can be accessed directly. Hence,
		// there is no need to create an object of the class.
		
		Employee refEmployee = new Employee();
		refEmployee.printEmployeeDetails2();
		// In order to access the non-static method of printEmployeeDetails2(), an
		// object of its class has to be created.
		
	}

}
