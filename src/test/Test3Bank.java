// Day 2 - Bank class
package test;

public class Test3Bank {
	private int bankID; // Global variable
	private String bankName; // Global variable
	
	// Shortcut to generate Getters and Setters:
	// Right click > Source > Generate Getters and Setters ...
	
	public int getBankID() {
		return bankID;
		// returns the value of the global variable
	}
	
	public void setBankID(int bankID) { // Parameter is local variable
		this.bankID = bankID;
		// Global variable (private int bankID) is assigned the value of
		// the local variable
	}
	
	public String getBankName() {
		return bankName;
		// returns the value of the global variable
	}
	
	public void setBankName(String bankName) { // Parameter is local variable
		this.bankName = bankName;
		// Global variable (private String bankName) is assigned the value of 
		// the local variable
	}
	
	
}
