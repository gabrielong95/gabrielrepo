package day17;

class Producer extends Thread {
	StringBuffer refData;
	
	Producer() {
		refData = new StringBuffer(); // Constructs the refData with no characters in it
	}
	
	@Override
	public void run() {
		synchronized (refData) {
			for (int i = 0; i < 3; i++) {
				try {
					refData.append(i + " ");
					Thread.sleep(1500);
					System.out.println("Appending data to string buffer . . .");
					refData.notify();
					
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				System.out.println("");
			} // End of for loop
		} // End of synchronized (refData)
	} // End of run()
} // End of Producer


class Consumer extends Thread {
	Producer refProducer;
	
	public Consumer(Producer refProducer) {
		this.refProducer = refProducer;
	}
	
	@Override
	public void run() {
		synchronized (refProducer.refData) {
			try {
				
			} catch (Exception e) {
				
			}
		}
	}
}



public class Example60 {

	public static void main(String[] args) {
		Producer refProducer = new Producer();
		
		Thread refThread1 = new Thread(refProducer);
		
		refThread1.start();

	}

}
