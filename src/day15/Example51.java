package day15;

// var as local variable since Java 10
// limitations : can't be used as Global Variable


public class Example51 {

	 // var globalVariable = 100;		// compilation error
	
	public static void main(String[] args) {
		
		var variable1 = 10;				// int
		var variable2 = "Hello";		// String
		var variable3 = 10.50f;			// float
		var variable4 = 10000000000L;	// long
		var variable5 = 'A';			// char
		var variable6 = 0;				// byte
		var variable7 = 1;				// short
		var variable8 = true;			// boolean
		
		
		EmployeeLocal refEmployeeLocal = new EmployeeLocal();
		var variable9 = refEmployeeLocal;
		
		System.out.println(variable1 + " "+variable2+ " "+variable3);
		System.out.println(variable4 + ""+variable5+ " "+variable6);
		System.out.println(variable7 + " "+variable8+ " "+variable9);
	}
	
}

class EmployeeLocal{
	@Override
	public String toString() {
		return "Gosling";
	}
}