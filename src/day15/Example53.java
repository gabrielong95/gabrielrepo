package day15;

class Demo3{
	
	// var ref = "hello";					// compilation error as instance variable
	
	// static var ref = "hello";			// compilation error as static or class variable
	
	// Demo3(var args){						// can't be used as parameter of constructor
		
	// }
	
	 //void methodDemo3(var args) {			// can't be used as parameter of method
	
	 //}
	
	void methodDemo3() {
		int number1 = 10;
		{
			var number2 = false;
			{
				System.out.println(number2);
			}
			System.out.println(number1);
		}
		 // System.out.println(number2);		// compilation error
	}
}


public class Example53 {

	public static void main(String[] args) {
		Demo3 refDemo3 = new Demo3();
		refDemo3.methodDemo3();
	}

}
