package day15;

class OCPJava11{
	int count;
	{
		System.out.println(count);	// 0
	}
	
	public OCPJava11() {
		count = 10;
		String x = "line 11";
		System.out.println(count + " " +x );
	}
}

public class Example54 {

	public static void main(String[] args) {
		
		var ref = new OCPJava11();
		System.out.println(ref.count);			// guess the output

	}

}
