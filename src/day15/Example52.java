package day15;

// // var is used as local variable in a constructor, non static block, static block and method

class VarDemo1{
	public void var1() {
		var var = "var";					// var as a variable name ==> no compilation error, but it is not a best coding practice
		System.out.println(var);
	}
	
	public static void var2() {
		VarDemo1 var = new VarDemo1();		// var as a variable name ==> no compilation error, but it is not a best coding practice
		var.var1();
	}
} // end of VarDemo1

class VarDemo2{
	
	
	VarDemo2(){
		var number3 = 30;
		System.out.println("Inside constructor: "+number3);
	} // end of constructor
	
	{
		var number1 = 10;
		System.out.println("Inside non-static block: "+number1);
		//example();
	} // end of non-static block
	
	static {
		var number2 = 20;
		System.out.println("Inside static block: "+number2);
		//example();
	} // end of static block
	
//	public static void example() {
//		System.out.println("printing from example");
//	}
	
} // end of VarDemo2



public class Example52 {

	public static void main(String[] args) {
		
	//	VarDemo1.var2();
		new VarDemo2();

	}

}
