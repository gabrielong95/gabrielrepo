package day15;

// Try to use lambda expression using runnable as innerclass

class ThreadDemo1 extends Thread {
	
	@Override
	public void run() {
		System.out.println("ThreadDemo1 -> Hello from run method().. ");
	}
	
} // End of ThreadDemo1

class ThreadDemo2 implements Runnable {
	
	
	
	@Override
	public void run () {
		System.out.println("ThreadDemo2 -> Hello from run method().. ");
	}
	
	
	
} // End of ThreadDemo2

public class Example56 {

	public static void main(String[] args) {
		
		ThreadDemo1 refThreadDemo1 = new ThreadDemo1();
		Thread refThread = new Thread(refThreadDemo1);
		
		refThread.start();
		
		ThreadDemo2 refThreadDemo2 = new ThreadDemo2();
		Thread refThread2 = new Thread(refThreadDemo2);
		refThread2.start();
		
		Runnable refRunnable = new Runnable() {
			
			@Override
			public void run() {
				System.out.println("Runnable -> Hello from run method().. ");
			}
		};
		
		Thread refThread3 = new Thread(refRunnable);
		refThread3.start();
		
		Runnable lambda = () -> {
			System.out.println("lambda");
		};
		
		Thread refLambda = new Thread(lambda);
		refLambda.start();
		
	} // End of main()

} // End of Example56

