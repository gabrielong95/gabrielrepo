package day9pack1;

public class User {
	
	public void getData1() {
		System.out.println("I am in public void getData1()");
	}
	
	private void getData2() {
		System.out.println("I am in private void getData2()");
	}
	
	protected static void getData3() { // What if there's no static keyword?
		System.out.println("I am in protected void getData3()");
		
		// If no static keyword, you cannot access from another package
		// 
	}
	
	void getData4() {
		System.out.println("I am in void getData4() aka default");
		// Don't have to explicitly call a default method "default"
		// If there is no access modifier, it will be default by itself
	}
} // End of User class
