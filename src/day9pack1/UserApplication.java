package day9pack1;

public class UserApplication {

	public static void main(String[] args) {
		
		User refUser = new User();
		
		refUser.getData1();
		refUser.getData3();
		refUser.getData4();
		
		
		// refUser.getData2();
		// Returns compilation error as private method/variable cannot be accessed outside the class and packages
		
		
	} // End of main()

} // End of userApplication class
