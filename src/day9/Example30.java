package day9;

import java.util.Scanner;

// do while loop

class UserInput {
	
}

public class Example30 {

	public static void main(String[] args) {
		
		String userOption = null;
		
		do {
		Scanner refScanner = new Scanner (System.in);
		
		System.out.println("Enter your name: ");
		String userName = refScanner.next();
		
		System.out.println("Do you wish to continue (yes/no): ");
		userOption = refScanner.next();
		
		} while (!userOption.equals("no"));	
		
		System.out.println("Do-while loop has ended!");
		
		
		

	}

}
