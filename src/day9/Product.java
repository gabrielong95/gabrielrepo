package day9;
// From Example32
public class Product {
	
	int productID;
	String productName;
	
	Product (int productID, String productName) {
		this.productID = productID;
		this.productName = productName;
	}
	
	@Override
	public String toString() {
		return ("Product ID: "+productID + "\nProduct Name:"+productName+"\n");
	}
	
}
