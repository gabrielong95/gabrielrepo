package day9;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;

// Continuation from Example28

public class Example29 {

	public static void main(String[] args) throws IOException {
		BufferedReader refBufferedReader = new BufferedReader(new InputStreamReader(System.in));
		
		System.out.println("Enter a number: ");		
		// int number = refBufferedReader.read();
		
		int number = Integer.parseInt(refBufferedReader.readLine());
		System.out.println(number);
		
		double number1 = Double.parseDouble(refBufferedReader.readLine());
		System.out.println(number1);
		// The above method of using Wrapper Class also applies for Float (Float.parseFloat()), Long (Long.parseLong(), etc.)
	}

}
