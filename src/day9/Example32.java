package day9;

import java.util.Scanner;

public class Example32 {

	public static void main(String[] args) {
		Scanner refScanner = new Scanner (System.in);
		
		System.out.println("Enter amount of products to insert to cart: ");
		int numOfProducts = refScanner.nextInt();
		
		// We have created an array of Product class
		Product refProduct[] = new Product[numOfProducts];
		
		for (int i = 0; i < refProduct.length; i++) {
			System.out.println("Enter product ID: ");
			int productID = refScanner.nextInt();
			refScanner.nextLine();
			System.out.println("Enter product name: ");
			String productName = refScanner.nextLine();
			
			//refScanner.nextLine(); // "Clears" the buffer that was read from nextInt();
			
			/*
			 * https://stackoverflow.com/questions/26626106/why-string-inputs-after-integer-input-gets-skipped-in-java
			 * because the nextInt method doesn't read the newline character of your input
			 * so when you issue the command nextLine, the Scanner finds the newline character and gives you that as a line.
			 */
			
			refProduct[i] = new Product(productID, productName);
		}

		for (Product x : refProduct) {
			System.out.println(x);
		}
	}
}
