package day9;

import java.util.Iterator;

public class Example31 {

	public static void main(String[] args) {
		
		/*
		 * Array is created on the dynamic memory 
		 * Array can be used to fetch any
		 * record (or faster execution) 
		 * Array ALWAYS starts at index 0 (i.e. from 0)
		 */		
		
		/* -------------------- ADVANTAGES OF JAVA ARRAY --------------------*\
		 * + Code Optimization: Easy to fetch and sort data 
		 * + Random Access: Can get any data located at any position
		 */		
		
		/* -------------------- DISADVANTAGES OF JAVA ARRAY --------------------*\
		 * - Size Limit: Can only store fixed elements in the array; doesn't grow its size at runtime
		 * - Collection framework solves the above problem
		 */	
		
		// Single dimension (1D) & multi-dimension array (2D, 3D, ...)
		
		// Creating a single dimension (1D) array:
		int productNumber[] = {10, 20, 30, 40, 50}; 

		for (int i : productNumber) {
			System.out.println(i); // Fetch value <i> from the given array
		}
		
		
		// By declaring the array first, then allocating the memory for it by using the "new" operator
		int productKey[] = new int[5];
	
		productKey[0] = 10;
		productKey[1] = 20;
		productKey[2] = 30;
		productKey[3] = 40;
		productKey[4] = 50;
		
		
		for (int i : productKey) {
			System.out.println("\nBy using new operator: ");
			System.out.println(i); // Fetch value <i> from the given array
		}
	}

}
