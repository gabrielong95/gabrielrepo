//Example40

package day12;
import java.io.IOException;

class MyClass{
	
	void myMethod1() throws IOException{ // checked exception
		throw new IOException();		 // user defined exception
	}
	
	void myMethod2() throws IOException { // compiled time exception
		myMethod1();
	}
	
	void myMethod3() {
		try {
			myMethod2();
		} catch (IOException e) {
			System.out.println("Exception Handled 1..");
		}
	}
}
public class ExceptionDemo03 {

	public static void main(String[] args) {
		MyClass refMyClass = new MyClass();
		refMyClass.myMethod3();		// final call		==> line15
		// no need of write down try catch here, since myMethod3() has try catch
		// we should avoid call myMethod2() or myMethod1() directly, why?? we need to handle exception again.
	}

}
