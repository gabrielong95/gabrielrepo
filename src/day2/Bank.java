package day2;

public class Bank {
	
	private int bankID; // Global variable
	private String bankLocation; // Global variable
	
	
	// Shortcut to generate Getters and Setters:
	// Right click > Source > Generate Getters and Setters ...
	
	public int getBankID() {
		return bankID;
		// returns the value of the global variable
	}
	
	public void setBankID(int bankID) { // Parameter is local variable
		this.bankID = bankID;
		// Global variable (private int bankID) is assigned the value of
		// the local variable
	}
	
	
	public String getBankLocation() {
		// returns the value of the global variable
		return bankLocation;
	}
	
	public void setBankLocation(String bankLocation) { // Parameter is local variable
		this.bankLocation = bankLocation;
		// Global variable (private String bankName) is assigned the value of 
		// the local variable	
	}
	
	
}
