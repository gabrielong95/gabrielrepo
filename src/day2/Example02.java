package day2;

class Employee { // Class name
	// Properties that belong to the Employee class (attributes/variables)
	static int employeeID = 10234; // --> Static variable.
	
	String employeeName = "Non-static variable name"; // --> Non-static variable.
	
	static void employeeDetails1() {		// Referring to a verb/action in English. --> Static method.
		System.out.println(employeeID);
		
		// System.out.println(employeeName); --> Returns compilation error. 
		// Non-static variable (String employeeName = ".....";) cannot be 
		// accessed from a  static method
		
	} // End of employeeDetails()
	
	
	
	void employeeDetails2() {
		System.out.println(employeeName); // --> Can access non-static variable.
		System.out.println(employeeID); // --> Can access static variable.
		
		// Non-static method can access static and non-static variables
		// But to access a non-static method, you have to create an object!
		
	}
} // End of Employee class

public class Example02 {
	static String data1 = "xyz";
	static String data2 = "123";
	String data3 = "non-static";
	public static void main(String[] args) {
		// What is static
		//------------------------------MEANING OF STATIC------------------------------\\
		// You don't have to create objects. In case of Java programming language, 
		// static means that a developer does not need to create an object of the class if 
		// the variable is static. We can access static variable or method directly by  \\
		// providing the class name ==> ClassName.variablename or ClassName.methodname()
		
		// What is non-static
		//------------------------------MEANING OF NON-STATIC------------------------------\\
		// We need to create an object of that class to access non-static variables or method
		
		System.out.println(data1);
		System.out.println(data2);
		
		// System.out.println(data3); --> Returns compilation error, static method
		// cannot access non-static variable!		
		
		Employee.employeeDetails1();
		// Static method of printEmployeeDetails() can be accessed directly. Hence,
		// there is no need to create an object of the class.	
		System.out.println("\n\n");
		
		
		
		// --------------------CREATING AN OBJECT AND WHY TO CREATE AN OBJECT --------------------\\
		// Since main method is static and we are trying to access a non-static method, then we 
		// have to create an object \\
		Employee emp2 = new Employee();
		emp2.employeeDetails2();
		// In order to access the non-static method of printEmployeeDetails2(), an
		// object of its class has to be created.
		
	}

}
