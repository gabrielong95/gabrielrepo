package day2;

public class Example03 {

	public static void main(String[] args) {
		
		Bank refBank = new Bank();
		// Creating a reference called refBank for new object of Test3Bank()
		
		refBank.setBankID(111);
		refBank.setBankLocation("CBP");
		// Using the reference (of Test3Bank class) to call the setters of 
		// the Test3Bank() object, which passes some value as parameters.
		
		
		System.out.println(refBank.getBankID());
		System.out.println(refBank.getBankLocation());
		// Using the reference (of Test3Bank class) to call the getters of 
		// the Test3Bank() object, which returns some value(s)		
	}

}
