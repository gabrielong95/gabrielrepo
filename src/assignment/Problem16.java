package assignment;

import java.util.Scanner;

public class Problem16 {

	public static void main(String[] args) {
		Scanner refScanner = new Scanner(System.in);
		
		System.out.println("Enter number of Fibonacci Series to print: ");
		int numberOfSeries = refScanner.nextInt();
		
//		System.out.println("Generating Fibonacci Series using for loop: ");
//		usingForLoop(numberOfSeries);
		
		System.out.println("Generating Fibonacci Series using while loop: ");
		usingForLoop(numberOfSeries);
	}
	
	public static void usingForLoop(int numberOfSeries) {
		int j = 0 , k = 1;
		for (int i = 1; i <= numberOfSeries; i++) {
			System.out.println(j + " ");
			int f = j + k;
			j = k;
			k = f;
		}
	}
	
	public static void usingWhileLoop(int numberOfSeries) {
		int index = 1;
		int j = 0, k = 1;
		
		while (index <= numberOfSeries) {
			System.out.println(j + ", ");
			
			int f = j + k;
			j = k;
			k = f;
			
			index++;
		}
	}

}
