// Problem1
/* Prompt user for n number of rows and print the following pattern:
 * *
 * **
 * ***
 * ****
 * *****
 */
package assignment;

import java.util.Scanner;

public class Problem1 {
	public static void main (String[] args) {
		Scanner refScanner = new Scanner (System.in);
		
		// Prompt and get n number of rows from user
		System.out.println("Enter number of rows you want to print: ");
		int numOfRows = refScanner.nextInt();
		
		// Initialize for loop to loop through the row
		for (int i = 0; i < numOfRows; i++) {
			
			// Initialize for loop to loop through the column
			for (int j = 0;  j <= i; j++) {
				
				System.out.print('*');
				} // End of column for() loop
			
			System.out.println(""); // Prints a line once for loop column is done
			} // End of row for() loop
		
	} // End of main
	
} // End of Problem1 Class