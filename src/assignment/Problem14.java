package assignment;

import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.Scanner;
//YET TO INSERT COMMENTS

/*
 * Conditions left to check:
 * forget password for non-existing account
 * forget password but incorrect email/security key
 * 
 */


public class Problem14 {
	static ArrayList<Problem14User> newUser = new ArrayList<Problem14User>();
	static Problem14User refProblem14User;
	static Scanner refScanner = new Scanner(System.in);

	public static void main(String[] args) {
		displayOption();
	}

	public static void displayOption() {
		int userOption;

		do {
			System.out.println("User Home Page:");
			System.out.println("1. Register");
			System.out.println("2. Login");
			System.out.println("3. Forget Password");
			System.out.println("4. Logout (exit)\n");

			System.out.print("Enter Your Choice: ");
			userOption = refScanner.nextInt();
			refScanner.nextLine();
			if (userOption < 1 || userOption > 4) {
				System.out.println("\nPlease enter 1 - 4 only.\n");
				continue;
			}

			switch (userOption) {
			case 1:
				firstOption();
				break;
			case 2:
				secondOption();
				break;
			case 3:
				thirdOption();
				break;
//			default:
//				System.out.println("Logout Successfully!!");
//				break;
			}
		} while (userOption != 4);
		System.out.println("Logout Successfully!!");
	}

	public static void firstOption() {
		String emailAddress, password, retypePassword, securityKey;
		Problem14User refNewUser = new Problem14User();

		// How to make it loop
		System.out.print("Enter email address: ");
		emailAddress = refScanner.nextLine();
		refNewUser.setEmailAddress(emailAddress);

		// Condition check for existing email
		for (Problem14User problem14User : newUser) {
			// Loops until no 2 same email exists
			while (problem14User.getEmailAddress().equals(emailAddress)) {
				System.out.println("email exists");
				System.out.print("Enter email address: ");
				emailAddress = refScanner.nextLine();
				refNewUser.setEmailAddress(emailAddress);
			}
		}
		// This section contains flaws.....

		System.out.print("Enter Password: ");
		password = refScanner.nextLine();
		refNewUser.setPassword(password);

		do {
			System.out.print("Re-type Password: ");
			retypePassword = refScanner.nextLine();

			if (!password.equals(retypePassword)) {
				System.out.println("Password doesn't match!!");
			}
		} while (!password.equals(retypePassword));

		System.out.print("What is your favourite clour? ");
		securityKey = refScanner.nextLine();
		refNewUser.setSecurityKey(securityKey);

		System.out.println(securityKey + " is your security key, incase if you forget your password.\n");

		newUser.add(refNewUser);

		System.out.println("Registration Successful!!\n");

//		/* Print values to check..... */
//		for (Problem14User problem14User : newUser) {
//			System.out.println(problem14User);
//		}
	}

	public static void secondOption() {

		// Condition to check if list is empty. If true, prompt user to register account
		// first
		if (newUser.isEmpty()) {
			System.out.println("\nNo accounts registered. Please register an account!\n");
		} else {
			System.out.print("Enter User ID: "); // Prompt and get user ID (i.e. registered email)
			String userid = refScanner.nextLine();

			System.out.print("Password: "); // Prompt and get user password
			String password = refScanner.nextLine();

			char toContinue = 'y'; // Initialize char to 'y' for dowhile loop during transaction. Loop stops after
									// user input 'n'.
			double currentAmount = 0, addAmount = 0, withdrawAmount = 0;

			for (Problem14User problem14User : newUser) {
				if (problem14User.getEmailAddress().equals(userid) && problem14User.getPassword().equals(password)) {
					System.out.println("\nLogin Successful!!\n");

					System.out.println("Type 1: Check Available Bank Balance");
					System.out.println("Type 2: Deposit Amount");
					System.out.println("Type 3: Withdraw Amount\n");

					do {
						System.out.print("Enter Your Choice: ");
						int choice = refScanner.nextInt();

						if (choice < 1 || choice > 3) {
							System.out.println("Choice not available!!");
						}

						else if (choice == 1) {
							System.out.println(problem14User.getAccountBalance());
						} else if (choice == 2) {
							boolean negativeValue = true;
							while (negativeValue) {
								System.out.print("Enter Amount: ");
								addAmount = refScanner.nextDouble();

								if (addAmount < 1) {
									negativeValue = true;
									System.out.println("Amount can't be negative!!");
								} else {
									negativeValue = false;
									System.out.println(addAmount + " dollar deposited successfully!!");
									currentAmount += addAmount;
									problem14User.setAccountBalance(currentAmount);
								}
							}
						} else if (choice == 3) {
							System.out.print("Enter Amount: ");
							withdrawAmount = refScanner.nextDouble();

							if (withdrawAmount > problem14User.getAccountBalance()) {
								System.out.print("Sorry!! insufficient balance.\n");
							}

							else if (withdrawAmount < problem14User.getAccountBalance()) {
								problem14User.setAccountBalance(currentAmount - withdrawAmount);
								System.out.println("Transaction Successful!!");
							}
						}

						try {
							System.out.print("Wish to Continue? (y/n): ");
							toContinue = refScanner.next().charAt(0);
						} catch (InputMismatchException e) {
							System.out.println(e);
						} // End of try catch 

					} while (toContinue != 'n'); // End of do while loop
					System.out.println("Thanks for Banking with Us !!!");
					break;

				} else {
					System.out.println("\nAccount doesn't exist.\n");
				}
				break;
			}
		} // End of if else 
	} // End of secondOption()

	public static void thirdOption() {
		String id, securitykey;
		Scanner refScanner = new Scanner(System.in);

		// Checks if array list is empty
		if (newUser.isEmpty()) {
			System.out.println("\nNo accounts registered. Please register an account!\n");
		} else {
			// If not empty then prompt ID and security key
			System.out.print("Enter Your ID: ");
			id = refScanner.nextLine();
			System.out.print("Enter security key: ");
			securitykey = refScanner.nextLine();
			String pass, retype;
			for (Problem14User problem14User : newUser) {
				if (problem14User.getEmailAddress().equals(id) && problem14User.getSecurityKey().equals(securitykey)) {
					do {
						System.out.print("Enter new password: ");
						pass = refScanner.nextLine();

						System.out.print("Retype password: ");
						retype = refScanner.nextLine();
					} while (!pass.equals(retype));
					problem14User.setPassword(pass);
					continue;
				// Check for incorrect email / incorrect security key
				} 
				else {
					// Check if account exists
					System.out.println("\nAccount doesnt exist.\n");
				}
				// End of if else
			} // End of for loop
		} // End of if else 
	} // End of thirdOption()
} // End of Problem14