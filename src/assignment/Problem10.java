// Problem10
// How to find common elements in three sorted arrays?

package assignment;

import java.util.Arrays;
import java.util.Scanner;

public class Problem10 {
	public static void main (String[] args) {
		Scanner refScanner = new Scanner (System.in);
		
		System.out.println("Enter 3 arrays of integer (in ascending order) to find the common elements");
		
		// Prompt user for number of integers to initialize the first array
		System.out.print("Enter number of integers to input for first array: ");
		int firstArray = refScanner.nextInt();
		int input1[] = new int[firstArray];
		
		for (int i = 0; i < input1.length; i++) {
			System.out.print("Enter an integer: ");
			input1[i] = refScanner.nextInt();
		}
		
		// Prompt user for number of integers to initialize the second array
		System.out.print("Enter number of integers to input for second array: ");
		int secondArray = refScanner.nextInt();
		int input2[] = new int[secondArray];
		
		for (int j = 0; j < input2.length; j++) {
			System.out.print("Enter an integer: ");
			input2[j] = refScanner.nextInt();
		}
		
		
		// Prompt user for number of integers to initialize the third array
		System.out.print("Enter number of integers to input for third array: ");
		int thirdArray = refScanner.nextInt();
		int input3[] = new int[thirdArray];
		
		for (int k = 0; k < input3.length; k++) {
			System.out.print("Enter an integer: ");
			input3[k] = refScanner.nextInt();
		}

		// Sort the 3 arrays
		Arrays.sort(input1); Arrays.sort(input2); Arrays.sort(input3);
		
		// Initialize index to 0
		int i = 0, j = 0, k = 0;
		
		// Loop until the longest length of array has been checked
		while (i < input1.length && j < input2.length && k < input3.length) {
			if (input1[i] == input2[j] && input2[j] == input3[k]) {
				System.out.print(input1[i] + " "); // Print out the index that has the same value as the others
				i++; j++; k++; // Increment the index of each array
			} else if (input1[i] < input2[j]) {
				i++;
			} else if (input2[j] < input3[k]){
				j++;
			} else 
				k++;
		}
	}

}
