// Problem2
/* Prompt user for n number of rows and print the following pattern:
 * 1
 * 12
 * 123
 * 1234
 * 12345
 */
package assignment;

import java.util.Scanner;

public class Problem2 {
	public static void main (String[] args) {
		Scanner refScanner = new Scanner (System.in);
		
		System.out.println("Enter number of rows you want to print: ");
		int numOfRows = refScanner.nextInt();
		
		// Initialize for loop to loop through the row
		for (int i = 0; i < numOfRows; i++) {
			
			// Initialize for loop to loop through the column
			for (int j = 0;  j <= i; j++) {
				System.out.print(j+1);
			} // End of for loop column
			System.out.println(""); // Prints a line once for loop column is done
		} // End of for loop row
		
	} // End of main
	
} // End of Problem2 Class
