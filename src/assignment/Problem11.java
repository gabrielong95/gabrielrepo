// Problem11
// Remove duplicates from an array

package assignment;

import java.util.Arrays;
import java.util.Scanner;

// YET TO INSERT COMMENTS
public class Problem11 {
	public static void main (String[] args) {
		Scanner refScanner = new Scanner (System.in);
		
		// Prompt user for length of array
		System.out.print("Enter number of elements to store in an array: ");
		int arrayLength = refScanner.nextInt();
		
		// Initialize array of size n
		int input1[] = new int[arrayLength];
		
		// Prompt user to store data in the array
		for (int i = 0; i < input1.length; i++) {
			input1[i] = refScanner.nextInt();
		}
		
		
		int length = removeDuplicates(input1, arrayLength);
		System.out.println("Removed Duplicates: ");
		for (int i = 0; i < length; i++) {
			System.out.print(input1[i] + " ");
		}
	}
	
	public static int removeDuplicates(int input1[], int arrayLength) {
		int j = 0;
		//Initialize new array to store removed duplicates
		int tempArray[] = new int[arrayLength];
		
		
		for (int i = 0; i < arrayLength - 1; i++) {
			if (input1[i] != input1[i+1]) { // If index 0 of input1 array is not the same as index 1,
				tempArray[j++] = input1[i]; // Store that element into new array
			}
		}
		
		
		tempArray[j++] = input1[arrayLength-1];
		
		for (int i = 0; i < j; i++) {
			input1[i] = tempArray[i];
		}
		
		return j;
		
	}

}
