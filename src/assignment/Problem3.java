// Problem3
/* Prompt user for n number of rows and print the following pattern:
 * A
 * AB
 * ABC
 * ABCD
 * ABCDE
 */
package assignment;

import java.util.Scanner;

public class Problem3 {
	public static void main (String[] args) {
		Scanner refScanner = new Scanner (System.in);
		
		System.out.println("Enter number of rows you want to print: ");
		int numOfRows = refScanner.nextInt();
		
		char first = 'A';
		int asciiFirst = first;
		
		// Initialize for loop to loop through the row
		for (int i = 0; i < numOfRows; i++) {
			// Initialize for loop to loop through the column
			for (int j = 0;  j <= i; j++) {
				System.out.print((char)(asciiFirst)); // Prints the character of int ascii
			}
			asciiFirst++; // Increase the int of char, i.e. A -> B -> C
			System.out.println(""); // Prints a line once for loop column is done
		}
	}
}
