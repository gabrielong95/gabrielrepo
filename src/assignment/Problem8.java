package assignment;

import java.util.Scanner;

public class Problem8 {

	public static void main(String[] args) {
		Scanner refScanner = new Scanner (System.in);
		System.out.println("Enter number of rows you want to print: ");
		int numOfRows = refScanner.nextInt();
		
		// Initialize for loop to loop through the row
		for (int i = 0; i < numOfRows; i++) {
			
			// Initialize for loop to loop through the column
			for (int j = numOfRows -1;  j >= i; j--) {
				System.out.print('*');
				if (j < 1) {
					System.out.print('*');
				}				
			} // End of for() loop
			System.out.println();
		} // End of for() loop

	} // End of main

} // End of Problem8 class
