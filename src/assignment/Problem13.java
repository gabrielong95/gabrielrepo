// Problem13
// Remove duplicate elements of a given array and return new length of array

package assignment;

import java.util.Scanner;
//YET TO INSERT COMMENTS

public class Problem13 {

	public static void main(String[] args) {
		Scanner refScanner = new Scanner (System.in);
		System.out.print("Enter number of elements to store in an array: ");
		int arrayLength = refScanner.nextInt();
		
		int input1[] = new int[arrayLength];
		
		for (int i = 0; i < input1.length; i++) {
			input1[i] = refScanner.nextInt();
		}		
		
		System.out.println("The new length of the array is: " +newLength(input1));
		
	}
	
	// Method to return new length of array after removing duplicates
	public static int newLength(int[] input1) {
		int index = 1;
		
		for (int i = 1; i < input1.length; i++) {
			if (input1[i] != input1[index-1]) {
				input1[index++] = input1[i];
			}
		}
		return index;
	}

}
