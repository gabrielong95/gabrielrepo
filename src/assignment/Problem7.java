package assignment;

import java.util.Scanner;

public class Problem7 {

	// Incomplete
	public static void main(String[] args) {
		Scanner refScanner = new Scanner (System.in);
		
		System.out.println("Enter number of rows: ");
		int numOfRows = refScanner.nextInt();
		
		int i, j, m;
		int k = 1;
		for (i = 1; i <= numOfRows; i++) {
			
			for (j = i;  j <= k; j++) {
				
				System.out.print(j);
			
			} // End of for() loop
			
			for (m = k; m > 1; m--) {
				System.out.println(m-1);
			}
			k+=2;
			System.out.println();
			}
		
		} // End of main
} // End of Problem7 class