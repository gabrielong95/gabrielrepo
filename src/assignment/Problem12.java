// Problem12
// Find the second largest and second smallest element in an Array

package assignment;

import java.util.Arrays;
import java.util.Scanner;

// YET TO INSERT COMMENTS

public class Problem12 {

	public static void main(String[] args) {
		Scanner refScanner = new Scanner (System.in);
		System.out.print("Enter number of elements to store in an array: ");
		int arrayLength = refScanner.nextInt();
		
		int input1[] = new int[arrayLength];
		
		for (int i = 0; i < input1.length; i++) {
			input1[i] = refScanner.nextInt();
		}
		
		// Sort the array
		Arrays.sort(input1);
		
		System.out.println("Second largest: "+findSecondLargest(input1, arrayLength));
		System.out.println("Second smallest: "+findSecondSmallest(input1, arrayLength));

	}
	
	// Method to find second largest
	public static int findSecondLargest(int[] input1, int arrayLength) {
		int tempLargest;
		
		for (int i = 0; i < input1.length+1; i++) {
			for (int j = i +1; j < input1.length; j++) {	
				if (input1[i] > input1[j]) {
					tempLargest = input1[i];
					input1[i] = input1[j];
					input1[j] = tempLargest;
				}
			}
		}
		return input1[arrayLength - 2];
		
	}
	
	// Method to find second smallest
	public static int findSecondSmallest(int[] input1, int arrayLength) {
		int tempSmallest;
		for (int i = 0; i < arrayLength; i++) {
			for (int j = i +1; j < arrayLength; j++) {
				if (input1[i] > input1[j]) {
					tempSmallest = input1[i];
					input1[i] = input1[j];
					input1[j] = tempSmallest;
				}
			}
		}
		return input1[1];
	}

}
