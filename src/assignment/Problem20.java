// Problem20
// Write a Java Program to check if a String is a palindrome or not
 // - Using reverse of a String and without reverse of a String
package assignment;

import java.util.Scanner;

public class Problem20 {

	public static void main(String[] args) {
		Scanner refScanner = new Scanner(System.in);
		System.out.print("Enter a string: ");
		String palindrome = refScanner.nextLine();
		
		usingReverse(palindrome);
		withoutReverse(palindrome);
		
	}

	public static void usingReverse(String palindrome) {
		// Method to check if a String is palindrome using reverse() method of StringBuilder
		StringBuilder isPalindrome = new StringBuilder();
		
		// isPalindrome = "" + palindrome
		isPalindrome.append(palindrome);
		// isPalindrome = palindrome;
		
		isPalindrome.reverse();
		if (palindrome.contentEquals(isPalindrome)) {
			System.out.println(palindrome + " is a palindrome");
		} else {
			System.out.println(palindrome + " is not a palindrome");
		}
	}
	
	public static void withoutReverse(String palindrome) {
		// Method to check if a String is palindrome without using reverse() method of StringBuilder
		String reverse = "";
		int lengthOf = palindrome.length();
		
		// Reverse the string from last index and assign it to the reverse String
		for (int i = lengthOf - 1; i >= 0; i--) {
			reverse = reverse + palindrome.charAt(i);
		}
		
		if (palindrome.equals(reverse)) {
			System.out.println(palindrome + " is a palindrome");
		} else {
			System.out.println(palindrome + " is not a palindrome");
		}
		
	}
}
