// Problem5
/* Prompt user for n number of rows and print the following pattern:
 * 12345
 * 1234
 * 123
 * 12
 * 1
 */

package assignment;

import java.util.Scanner;

public class Problem5 {
	public static void main(String[] args) {
		Scanner refScanner = new Scanner (System.in);
		
		System.out.println("Enter number of rows you want to print: ");
		int numOfRows = refScanner.nextInt();
		
		// Initialize for loop to loop through the row
		for (int i = 1; i <= numOfRows; i++) {
			
			// Initialize for loop to loop through the column
			// Initialize j as 1 so it will print 1 first
			for (int j = 1; j <= numOfRows-i+1; j++) {
				System.out.print(j);
			}
			System.out.println();
		}
	}	
}
