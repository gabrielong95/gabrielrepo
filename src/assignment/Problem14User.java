// Problem14User class 
// User object to be instantiated when userOption is 1
package assignment;

//import java.util.ArrayList;

public class Problem14User {
	//ArrayList<Problem14User> newUser = new ArrayList<Problem14User>();
	private String emailAddress, password, securityKey;
	double accountBalance = 0;
	Problem14User() {
		
	}
	
	Problem14User (String emailAddress, String password, String securityKey, double accountBalance) {
		this.emailAddress = emailAddress;
		this.password = password;
		this.securityKey = securityKey;
		this.accountBalance = accountBalance;
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getSecurityKey() {
		return securityKey;
	}

	public void setSecurityKey(String securityKey) {
		this.securityKey = securityKey;
	}
	
	public double getAccountBalance() {
		return accountBalance;
	}

	public void setAccountBalance(double accountBalance) {
		this.accountBalance = accountBalance;
	}
	
	@Override
	public String toString() {
		return emailAddress + " " + password + " " +securityKey + " " +accountBalance;
	}
	
}
