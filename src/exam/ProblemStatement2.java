package exam;

import java.util.Scanner;

public class ProblemStatement2 {

	public static void main(String[] args) {
		Scanner refScanner = new Scanner (System.in);
		
		int productID, quantity;
		String productName;
		double price;
		double totalPrice = 0;
		double discount = 0.2;
		char userOption;
		
		int arraySize = 100;
		Product refProduct[] = new Product [arraySize];
		
		for (int i = 0; i <= refProduct.length; i++) {
			
			System.out.print("Enter Product ID: ");
			productID = refScanner.nextInt();
			refScanner.nextLine();
			System.out.println();
			
			System.out.print("Product Name: ");
			productName = refScanner.nextLine();
			System.out.println();
			
			System.out.print("Price in SGD: ");
			price = refScanner.nextDouble();
			
			refScanner.nextLine();
			System.out.println();
			
			System.out.print("Quantity: ");
			quantity = refScanner.nextInt();
			
			double currentItemPrice = price * quantity;
			totalPrice += currentItemPrice;
			
			refScanner.nextLine();
			System.out.println();
			
			refProduct[i] = new Product (productID, productName, price, quantity);
			System.out.print("Wish to Continue: (Y/N): ");
			userOption = refScanner.next().charAt(0);
			userOption = Character.toLowerCase(userOption);
			System.out.println();
			
			if (userOption == 'n') {
				break;
			}
		
		}
	System.out.println();
	
	System.out.println("Product ID\tProduct Name\t\t\tPrice\t\t Quantity\n");
	
	for (Product product : refProduct) {
		if (product == null) {
			break;
		}

		System.out.println(product);
	}
	
	System.out.println("Total Price: "+totalPrice);
	System.out.println("Flat Discount: " +discount*100 +"%");
	System.out.println("Discount Amount "+(totalPrice * discount));
	System.out.println("Amount to Pay: "+(totalPrice - (totalPrice * discount)));
	}
}
