package exam;

public class Product {
	int productID, quantity;
	String productName;
	double price;
	
	Product (int productID, String productName, double price, int quantity) {
		this.productID = productID;
		this.productName = productName;
		this.price = price;
		this.quantity = quantity;
	}
	
	@Override
	public String toString() {
		return (productID +"\t\t"+productName+"\t\t\t"+price+"\t\t"+quantity+"\n");
	}

}
