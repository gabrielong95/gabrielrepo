package day9pack2;

import day9pack1.User;

public class UserApplication2 extends User {

	public static void main(String[] args) {

			User refUser = new User();
			
			refUser.getData1(); 
			// Public can be accessed anywhere
			
			
			User.getData3();
			// User.getData3();
			getData3();
			// By using static protected method using inheritance (extends), we can then 
			// access it outside the package
			
			// refUser.getData4();
			// Default methods cannot be called outside the package
			
			// getData2();
			// refUser.getData2();
			// Returns compilation error as private method/variable cannot be accessed outside the class and packages
			
	}

}
