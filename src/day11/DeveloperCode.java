package day11;

public class DeveloperCode {
	
	static void getNumber() {
		try {
			// Always put your code within the try { } block.
			// Write your logic and statements here
			int number = 10;
			System.out.println(number/0);
			} catch (Exception e) {
			// You catch the exception in the catch { } block.
			// Write your statement of exception here
			System.out.println("Number can't be divisible by 0!");
			}
		
		finally {
			System.out.println("File is closing ... ...");
			}
		}
}
