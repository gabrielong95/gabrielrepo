package day1;
// import java.io.PrintStream;

import java.io.PrintStream;
// Import the required classes from the library
import java.text.SimpleDateFormat;
import java.util.Date;


public class Example01 {

	public static void main(String[] args) {
		/* 
		 * Method signature: The name of a method and its parameter; 
		 * The ability to write methods that have the same name but different parameters
		 *
		 */
		
		System.out.println("Hello World!");
		
		/*
		 * Note:
		 * System is a class, out is an object; print() is a function of the PrintStream class
		 * System.out.println();
		 * System.out is pointing at some memory location, also known as the refPrintStream
		 * The concept of reference refers to memory location
		 */
		
		PrintStream refPrintStream = System.out;
		// Via the above line of code, we can use refPrintStream to print output now.
		refPrintStream.println("Hello World! Printed using refPrintStream");
		
		
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		// Creating a SimpleDateFormat object by passing in the required format of the date
		
		Date date = new Date();
		System.out.println(sdf.format(date));
		
	}
}
