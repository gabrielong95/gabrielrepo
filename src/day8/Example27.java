package day8;

// Type Casting

/* 8 primitive data types
 * byte, short, char, int, long, float, double, boolean
 */


// Wrapper Class (since Java Collection Framework doesn't accept any primitive data types, it only accepts classes and Wrapper classes)
// Byte, Short, Char, Int, Long, Float, Double, Boolean





class CastingDemo {
	
	void getData() {
		int number1 = 100;
		long number2 = number1; // Implicit casting done by JVM
	
		float number3 = 50;
		double number4 = number3; // Implicit casting done by JVM
	}
	
	void getData2() {
		long number5 = 10;
		int number6 = (int) number5; // Explicit casting done by Developer
	
		double number7 = 30;
		float number8 = (float) number7; // Explicit casting done by Developer
	}
	
	
	void getData3() {
		String info1 = "Hello";
		Object info2 = info1; // Upcasting done by JVM (String to object class)
		// Object class is a superclass of all the classes in java.
		Object info3 = "Hi";
		String info4 = (String) info3; // Down casting done by Developer
	}
	
	void getData4() {
		int number9 = 1000;
		Integer refInteger = number9; // Auto boxing done by JVM (primitive to wrapper)
	}
	
	void getData5() {
		Double refDouble = 500.123;
		double number10 = refDouble; // Unboxing done by JVM (Wrapper to primitive)
	}
	
}

public class Example27 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
