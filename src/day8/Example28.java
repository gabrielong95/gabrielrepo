package day8;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/* User Input can be done in the following ways:
 * a. By using Scanner class
 * b. By using Command Line Argument
 * c. By using Buffered Reader class
 * d. By using Console class
 */

// Q: How to take input from User by using BufferedReader Class

class BufferReaderDemo {
	void getName() throws IOException {
		BufferedReader refBufferedReader = new BufferedReader (new InputStreamReader(System.in));
		System.out.println("Enter your name: ");
		String name1 = refBufferedReader.readLine();		
		System.out.println(name1);
		
		System.out.println("Enter a character: ");
		char charData = refBufferedReader.readLine().charAt(3);
		System.out.println(charData);
	}
	
}

public class Example28 {
	
	public static void main(String[] args) throws IOException {
		new BufferReaderDemo().getName();
	}
}
