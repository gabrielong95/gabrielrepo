package day8;

// How to take input from the Eclipse IDE: Command Line Argument

/*
 * Control Statement
 * if-else, for loop, switch case
 * 
 * do while and while
 */

public class Example26 {

	public static void main(String[] args) {
		//String data[] = args;
		String data1 = args[2];
		// int anyName = 0; --> Concept does not take run time argument
		/*
		for (int i = 0; i < args.length; i++) {
			System.out.println(data[i]);
		}
		*/
		// int condition = Integer.parseInt(data1);
		
		switch (data1) {
		case "paris":
			System.out.println("Paris");
			break;
			
		case "tokyo":
			System.out.println("Tokyo");
			break;
		case "singapore":
			System.out.println("Singapore");
			break;
		default:
			System.out.println("Data not found!");
			break;
		}
		
	} // End of main() method

} // End of Example26 class
