package day5;

abstract class KIOSK {
	// Abstract means not allowed to write an abstract method within the body
	// Abstract class = declaration
	void payBill1() { // Concrete method; where your method has a body (where statements are written)
		// Body of the code is written here	
	}
	
	abstract void payBill2(); // Abstract method without having body (implementation)

}

class Starhub extends KIOSK {

	@Override
	void payBill2() {
		System.out.println("pay starhub bill..");
	}

} // End of Starhub class

class Singtel extends KIOSK {

	@Override
	void payBill2() {
		System.out.println("pay singtel bill..");
	}
} // End of Singtel class


public class Example17 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
