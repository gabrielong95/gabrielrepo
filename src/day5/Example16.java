package day5;

// According to concept of method of overriding, it has to be parent child relationship,
// method name must be same, parameter must be same, return type must also be same (otherwise will result in compilation error)

// However in co-variant return type, it allows us to have different return types (on class level)

class Company {
	
	Company refCompany = null;
	
	public Company getMethod() {
		refCompany = new Company();
		return refCompany;
	} // End of Company getMethod() 

	public int getNumber() {
		return 100;
	}
	
} // End of Company class


class Department extends Company {
	
	Department refDepartment = null;
	
	@Override
	public Department getMethod() {
		refDepartment = new Department();
		return refDepartment;
	} // End of Department getMethod()
	
	@Override
	public int getNumber() {
		return 200;
	}
} // End of Department class

public class Example16 {
	public static void main(String[] args) {
		System.out.println(new Department().getNumber());
	}

}
