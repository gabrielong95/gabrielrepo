package day5;

class Driver {
	
	// What is a constructor?
	// What is the difference between method and constructor?
	// Can constructor be written as private? If yes, what is the purpose?
	// The purpose is to write a singleton class, -> creates a single object
	// This improves performance as memory will not be compromised.
	
	private static Driver refDriver = null;
	
	private Driver() { // Security and performance purposes
		System.out.println("calling Driver constructor..");
	}
	
	public static Driver getMethod() {
		
		/*if (refDriver == null) { // null means not pointing to a memory address
			refDriver = new Driver();
		}*/

		return refDriver;
		
	} // End of getMethod()
	
} // End of Driver class

public class Example15 {
	private String name = "investment1";
	
	public Example15() {
		name = "investment2";
		//System.out.println("setting constructor");
	}
	
	public String toString() {
		return name;
	}
	public static void main(String[] args) {
		/*Driver.getMethod();
		Driver.getMethod();
		Driver.getMethod();
		Driver.getMethod();
		Driver.getMethod();*/
		Example15 ref15 = new Example15();
		Example15 ref2 = new Example15();
		System.out.println(ref2);
		
	}

}
