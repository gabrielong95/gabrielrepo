package day5;

abstract class Microsoft {
	abstract void getMicrosoftDriver();
} // End of Microsoft

abstract class Amazon {
	abstract void getAmazonDriver();
} // End of Amazon

abstract class Google {
	abstract void getGoogleDriver();
} // End of Google


class OptimumApplication {
	// Override all the abstract methods from Microsoft, Amazon, and Google and implement here.	
	// You cannot create an object of abstract class
	
	// Anonymous inner class (ends with semicolon) // Lambda expression
	// Able to override method without extends or implements
	Microsoft refMicrosoft = new Microsoft() {	
		@Override
		void getMicrosoftDriver() {
			System.out.println("Microsoft driver");
		}
	};
	
	Amazon refAmazon = new Amazon() {
		@Override
		void getAmazonDriver() {
			System.out.println("Amazon driver");
		}
	};
	
	Google refGoogle = new Google() {
		@Override
		void getGoogleDriver() {
			System.out.println("Google driver");
		}
	};
}


public class Example18 {
	public static void main(String[] args) {
		// Call optimum application and run here
		OptimumApplication oaRef = new OptimumApplication();
		oaRef.refMicrosoft.getMicrosoftDriver();
		oaRef.refAmazon.getAmazonDriver();
		oaRef.refGoogle.getGoogleDriver();
	}

}
