package banking.pojo;

public class Customer {

	private String emailAddress, password, securityKey;
	double accountBalance = 0;

	public Customer() {

	}

	Customer(String emailAddress, String password, String securityKey, double accountBalance) {
		this.emailAddress = emailAddress;
		this.password = password;
		this.securityKey = securityKey;
		this.accountBalance = accountBalance;
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getSecurityKey() {
		return securityKey;
	}

	public void setSecurityKey(String securityKey) {
		this.securityKey = securityKey;
	}

	public double getAccountBalance() {
		return accountBalance;
	}

	public void setAccountBalance(double accountBalance) {
		this.accountBalance = accountBalance;
	}

	@Override
	public String toString() {
		return emailAddress + " " + password + " " + securityKey + " " + accountBalance;
	}
}
