package banking.dao;

import banking.pojo.Customer;
import banking.utility.DBUtility;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

// import utility.

public class CustomerDAOImplementation implements CustomerDAO {

	Connection refConnection = null;
	PreparedStatement refPreparedStatement = null;
	Statement refStatement = null;

	@Override
	public void createUser(Customer refCustomer) {
		try {
			refConnection = DBUtility.getConnection();

			// MySQL Insert query
			String sqlQuery = "insert into customer(email_address, password, security_key, balance) values (?,?,?, 0.0)";

			// Create the MySQL Insert PreparedStatement
			refPreparedStatement = refConnection.prepareStatement(sqlQuery);
			refPreparedStatement.setString(1, refCustomer.getEmailAddress());
			refPreparedStatement.setString(2, refCustomer.getPassword());
			refPreparedStatement.setString(3, refCustomer.getSecurityKey());
			
						
			// 2 ways of executing the statement (of the query):

			// Approach 1:
			// refPreparedStatement.execute();

			// Approach 2:
			int record = refPreparedStatement.executeUpdate();
			if (record > 0)
				System.out.println("New record has been successfully inserted");

		} catch (SQLException e) {
			e.printStackTrace();
			System.out.println("Exception Handled while inserting record.");
		} 

	}

	@Override
	public void updatePassword(Customer refUser) {
		// TODO Auto-generated method stub

	}

}
