package banking.dao;

import banking.pojo.Customer;

public interface CustomerDAO {
	
	void createUser(Customer refCustomer); // Option 1
	
	void updatePassword(Customer refCustomer); // Option 3
	
	/*
	 * void getUserRecord();
	void insertUserRecord(User refUser); // REGISTER
	
	void updateRecord(User refUser); // UPDATE
	
	void userLogin(); // Exam // LOGIN
	 */
	
	
}
