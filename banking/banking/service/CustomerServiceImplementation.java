package banking.service;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.Scanner;

import banking.dao.CustomerDAOImplementation;
import banking.pojo.Customer;
import banking.dao.CustomerDAO;
import banking.pojo.Customer;

public class CustomerServiceImplementation implements CustomerService {
	CustomerDAO refCustomerDAO;
	Scanner refScanner;
	Customer refCustomer;

	Connection refConnection = null;

	static ArrayList<Customer> newCustomer = new ArrayList<Customer>();
	//static Customer refStaticCustomer;

	@Override
	public void createCustomerRecord() {
		String emailAddress, password, retypePassword, securityKey;
		double accountBalance = 0;
		Customer refNewCustomer = new Customer();
		refScanner = new Scanner(System.in);

		try {
			System.out.print("Enter email address: ");
			emailAddress = refScanner.nextLine();
			refNewCustomer.setEmailAddress(emailAddress);
			
			for (Customer customer : newCustomer) {

				while (customer.getEmailAddress().equals(emailAddress)) {
					System.out.println("email exists");
					System.out.print("Enter email address: ");
					emailAddress = refScanner.nextLine();
					refNewCustomer.setEmailAddress(emailAddress);
				}
			}
			
			System.out.print("Enter Password: ");
			password = refScanner.nextLine();
			refNewCustomer.setPassword(password);

			do {
				System.out.print("Re-type Password: ");
				retypePassword = refScanner.nextLine();

				if (!password.equals(retypePassword)) {
					System.out.println("Password doesn't match!!");
				}
			} while (!password.equals(retypePassword));

			System.out.print("What is your favourite clour? ");
			securityKey = refScanner.nextLine();
			refNewCustomer.setSecurityKey(securityKey);
			
			System.out.println(securityKey + " is your security key, incase if you forget your password.\n");
			newCustomer.add(refNewCustomer); // Add to array list
			
			refCustomer = new Customer();
			refCustomer.setEmailAddress(emailAddress);
			refCustomer.setPassword(password);
			refCustomer.setSecurityKey(securityKey);
			refCustomer.setAccountBalance(accountBalance);
			// What does this do?
			refCustomerDAO = new CustomerDAOImplementation();
			refCustomerDAO.createUser(refCustomer);
	
		} catch (InputMismatchException e) {
			System.out.println("Enter integer only.");
		}

	}

	@Override
	public void updateCustomerRecord() {
		// TODO Auto-generated method stub

	}
	
	@Override
	public void userChoice() {
		System.out.println("1. Register");
		System.out.println("2. ... ...");
		System.out.println("3. ... ...");
		System.out.println();
		System.out.print("Enter Choice: ");
		refScanner = new Scanner(System.in);
		int choice = refScanner.nextInt();

		switch (choice) {
		case 1:
			createCustomerRecord();
			break;
		default:
			System.out.println("Option not found..");
			break;
		}
	}
	
	
}
