package utility;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.DriverManager;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Properties;

// DriverManager: DriverManager class is used to register driver for a specific database type (e.g. MySQL)
// and to establish a database connection with the server via its getConnection() method.

// Connection: Connection interface is used to establish the database connection (session)
// from which we can create statements to execute queries and retrieve results


// Purpose of Utility class:
// To establish a connection with the dao layer and
// connection.properties file

// Acts as a layer between DAO and .properties file

public class DBUtility {
	private static final String DB_DRIVER_CLASS="driverClassName";
	private static final String DB_USERNAME="username";
	private static final String DB_PASSWORD="password";
	private static final String DB_URL ="url";
	
	private static Connection connection = null;
	private static Properties properties = null;
	static{
		try {
			properties = new Properties();
			properties.load(new FileInputStream("jdbc/dbConnection.properties")); // give the path and call .properties file
			Class.forName(properties.getProperty(DB_DRIVER_CLASS));
			connection = DriverManager.getConnection(
					properties.getProperty(DB_URL),properties.getProperty(DB_USERNAME) , properties.getProperty(DB_PASSWORD) );
			System.out.println("Success Connection..");
			//System.out.println(properties.getProperty(DB_URL));
		} catch (ClassNotFoundException | SQLException | IOException e) {
			e.printStackTrace();
		}
	} // end of static block

	public static Connection getConnection() {
		return connection;
	} // end of getConnection

}
