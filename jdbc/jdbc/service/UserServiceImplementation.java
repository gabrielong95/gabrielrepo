package jdbc.service;

import java.util.InputMismatchException;
import java.util.Scanner;

import jdbc.dao.UserDAO;
import jdbc.dao.UserDAOImplementation;
import jdbc.pojo.User;
import utility.DBUtility;

import java.sql.Connection;

public class UserServiceImplementation implements UserService {
	UserDAO refUserDao;
	Scanner refScanner;
	User refUser;

	Connection refConnection = null;
	@Override
	public void userReadRecord() {
		try {
			refUserDao = new UserDAOImplementation();
			refUserDao.getUserRecord();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	@Override
	public void userCreateRecord() {
		refScanner = new Scanner(System.in);
		
		try {
			System.out.println("Enter User ID: ");
			int userLoginID = refScanner.nextInt();

			System.out.println("Enter User Password: ");
			String userPassword = refScanner.next();

			refUser = new User();
			refUser.setUserID(userLoginID);
			refUser.setUserPassword(userPassword);

			// What does this do?
			refUserDao = new UserDAOImplementation();
			refUserDao.insertUserRecord(refUser);
		} catch (InputMismatchException e) {
			System.out.println("Enter integer only.");
		}
	}

	@Override
	public void userUpdateRecord() {
		refScanner = new Scanner(System.in);
		
		try {
			System.out.println("Enter User ID to update: ");
			int userLoginID = refScanner.nextInt();

			System.out.println("Enter new password: ");
			String userPassword = refScanner.next();

			refUser = new User();
			refUser.setUserID(userLoginID);
			refUser.setUserPassword(userPassword);

			// What does this do?
			refUserDao = new UserDAOImplementation();
			refUserDao.updateRecord(refUser);
		} catch (InputMismatchException e) {
			System.out.println("Enter integer only.");
		}
		
		
		
	}
	
	@Override
	public void userDeleteRecord() {
		refScanner = new Scanner(System.in);

		try {
			System.out.println("Enter User ID to delete: ");
			int userLoginID = refScanner.nextInt();

			System.out.println("Enter User Password to delete: ");
			String userPassword = refScanner.next();

			refUser = new User();
			refUser.setUserID(userLoginID);
			refUser.setUserPassword(userPassword);

			// What does this do?
			refUserDao = new UserDAOImplementation();
			refUserDao.deleteRecord(refUser);
		} catch (InputMismatchException e) {
			System.out.println("Enter integer only.");
		}

	}

	/*
	 * 
	 * void getUserRecord();
	void insertUserRecord(User refUser);
	void deleteRecord(User refUser);
	void updateRecord();
	
	void userLogin();
	 */
	
	
	@Override
	public void userChoice() {

		
		
		
		System.out.println("1. Create user record");
		System.out.println("2. Read user record");
		System.out.println("3. Update user record");
		System.out.println("4. Delete user record ");
		System.out.println("5. Login");
		System.out.println();
		System.out.print("Enter Choice: ");
		refScanner = new Scanner(System.in);
		int choice = refScanner.nextInt();

		switch (choice) {
		case 1:
			userCreateRecord();
			break;

		case 2:
			userReadRecord();
			break;

		case 3:
			userUpdateRecord();
			break;
		case 4:
			userDeleteRecord();
			break;
		case 5:
			// Login
			break;
		default:
			System.out.println("Option not found..");
			break;
		}

	}

}
