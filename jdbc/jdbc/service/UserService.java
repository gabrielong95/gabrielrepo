package jdbc.service;

public interface UserService {
	void userCreateRecord();
	void userReadRecord();
	void userUpdateRecord();
	void userDeleteRecord();

	void userChoice();
}
