package jdbc.dao;
// UserDAO - Implementation file.
// This is where you write your query. Your main should be kept clean

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;


import java.sql.Statement;

import jdbc.pojo.User;
import utility.DBUtility;

public class UserDAOImplementation implements UserDAO {
	
	Connection refConnection = null;
	PreparedStatement refPreparedStatement = null;
	Statement refStatement = null;
	
	
	// CREATE
	@Override
	public void insertUserRecord(User refUser) {
		try {
			refConnection = DBUtility.getConnection();
			
			// MySQL Insert query
			String sqlQuery = "insert into user(user_id, user_password) values (?,?)";
			
			// Create the MySQL Insert PreparedStatement
			refPreparedStatement = refConnection.prepareStatement(sqlQuery);
			refPreparedStatement.setInt(1,  refUser.getUserID());
			refPreparedStatement.setString(2,  refUser.getUserPassword());;
			
			// 2 ways of executing the statement (of the query): 
			
			// Approach 1:
			//refPreparedStatement.execute();
			
			//Approach 2:
			int record = refPreparedStatement.executeUpdate();
			if (record > 0) System.out.println("New record has been successfully inserted");
			
		} catch (SQLException e) {
			System.out.println("Exception Handled while inserting record.");
		} finally {
			try {
				refConnection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			finally {
				System.out.println("Closing Connection..");
			}
		}
		
	} // InsertRecord
	
	
	// READ
	@Override
	public void getUserRecord() {
		try {
			refConnection = DBUtility.getConnection();
			
			// MySQL Insert query
			String sqlQuery = "SELECT * FROM user";
			
			// Create the MySQL Insert PreparedStatement
			refStatement = refConnection.createStatement();

			
			ResultSet rs = refStatement.executeQuery(sqlQuery);
			int x = 1;
			while (rs.next()) {
				System.out.print("Record "+x+":"+"\n");
				System.out.println("ID: " +rs.getInt("user_id"));
				System.out.println("Password: " +rs.getString("user_password")+"\n");
				x++;	
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	@Override
	public void updateRecord(User refUser) {
		try {
			refConnection = DBUtility.getConnection();
			
			// MySQL Update query
			String sqlQuery = "UPDATE user set user_password=? where user_id = ?";
			
			// Create the MySQL Insert PreparedStatement
			refPreparedStatement = refConnection.prepareStatement(sqlQuery);			
			refPreparedStatement.setString(1, refUser.getUserPassword());
			refPreparedStatement.setInt(2,  refUser.getUserID());
			
			
			// Approach 1:
			//refPreparedStatement.execute();
			
			// Approach 2:
			int record = refPreparedStatement.executeUpdate();
			if (record > 0) System.out.println("Record has been successfully updated");
			
		} catch (SQLException e) {
			//e.printStackTrace();
			System.out.println("Exception Handled while updating record.");
		}
		finally {
			try {
				refConnection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			finally {
				System.out.println("Closing Connection..");
			}
		}
		
	}
	
	// DELETE
	@Override
	public void deleteRecord(User refUser) {
		try {
			refConnection = DBUtility.getConnection();
			
			// MySQL Insert query
			String sqlQuery = "delete from user where user_id = ?";
			
			// Create the MySQL Insert PreparedStatement
			refPreparedStatement = refConnection.prepareStatement(sqlQuery);			
			refPreparedStatement.setInt(1,  refUser.getUserID());
			
			// Approach 1:
			//refPreparedStatement.execute();
			
			// Approach 2:
			int record = refPreparedStatement.executeUpdate();
			if (record > 0) System.out.println("Record has been successfully deleted");
			
		} catch (SQLException e) {
			System.out.println("Exception Handled while deleted record.");
		}
		finally {
			try {
				refConnection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			finally {
				System.out.println("Closing Connection..");
			}
		}
		
	}
	

	
	@Override
	public void userLogin() {
		
	}
	
//	public void checkConnectionStatus() {
//		System.out.println("Connection Successful...");
//	}


}
