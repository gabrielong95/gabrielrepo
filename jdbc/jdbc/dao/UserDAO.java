package jdbc.dao;

import jdbc.pojo.User;

// UserDAO - Interface file

public interface UserDAO {
	
	void getUserRecord();
	void insertUserRecord(User refUser);
	void deleteRecord(User refUser);
	void updateRecord(User refUser);
	
	void userLogin(); // Exam
}
