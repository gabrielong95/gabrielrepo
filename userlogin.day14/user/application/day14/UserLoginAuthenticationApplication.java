package user.application.day14;

import user.controller.UserLoginController;

// classNameApplication
// Application implies that the main method can be found in this class.

// Application always calls service, controller, then service; In between Application and Service = Controller
// Service calls dao, then calls the get method

/*
 * ==========================
 * User Login Authentication *
 * ==========================
 * 
 * Flow of Application:
 * -------------------------
 * a. user application
 * 	b. user controller (Set the value to POJO - Plain Old Java Object)
 * 		c. user service -> more service = more controller
 * 			- user service interface
 * 			- user service Implementation
 * 			d. user dao (Data Access Object) / hibernate / Sprint Data JPA
 * 				- user dao interface
 * 				- user dao Implementation
 * 					4.1. get the value from POJO
 * 					4.2. Connect to Database (MySQL)
 * 
 * Top down approach -> line 16 to line 25
 * Bottom up approach -> line 25 to line 16
 * 
 */

// Application layers calls controller, then controller calls service.
// In service layer we set the values. After we set the values, then service calls DAO.
// In DAO, we get the values from POJO.

public class UserLoginAuthenticationApplication {

	public static void main(String[] args) {
		UserLoginController refUserLoginController = new UserLoginController();
		refUserLoginController.getUserLoginService();
		
	}

}
