// POJO - Plain Old Java Object
// POJO class = Entity class
// AKA encapsulated class (with encapsulated features)
// Encapsulated features = public class with private variables with getters and setters method

package user.pojo;

public class User {
	private String userLoginID;
	private String password;
	
	public String getUserID() {
		return userLoginID;
	}
	public void setUserLoginID(String userLoginID) {
		this.userLoginID = userLoginID;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}	
}
