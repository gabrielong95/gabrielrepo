package user.dao;

import user.pojo.User;

public class UserLoginDAOAuthenticationImplementation implements UserLoginDAOInterface {

	private boolean authenticationStatus = false;
	
	@Override
	public boolean userLoginAuthentication(User refUser) {
		
		if (refUser.getUserID().equals("admin") && refUser.getPassword().equals("admin123")) {
			authenticationStatus = true;
		} else {
			authenticationStatus = false;
		}
		
		return authenticationStatus;
	}

}
