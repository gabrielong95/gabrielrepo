package user.service;

import java.util.Scanner;

import user.dao.UserLoginDAOAuthenticationImplementation;
import user.dao.UserLoginDAOInterface;
import user.pojo.User;

public class UserLoginServiceImplementation implements UserLoginServiceInterface {
	
	UserLoginDAOInterface refUserLoginDAOInterface = null;
	User refUser;
	
	@Override
	public void callUserDAOImplementation() {
		
		refUser = new User();
		
		Scanner refScanner = new Scanner(System.in);
		
		System.out.println("Enter user name: ");
		String name = refScanner.next();
		refUser.setUserLoginID(name); // it will call and set the value to User POJO class file
		
		System.out.println("Enter password: ");
		String password = refScanner.next();
		refUser.setPassword(password); // it will call and set the value to User POJO class file
		
		refUserLoginDAOInterface = new UserLoginDAOAuthenticationImplementation();
		
		
		refUserLoginDAOInterface.userLoginAuthentication(refUser); // if this condition returns true, then call user dash board.
																	// else redirect to user login home page
		
		// write ifelse
		if (refUserLoginDAOInterface.userLoginAuthentication(refUser) == true) {
			System.out.println("Authenticated.");
		} else {
			System.out.println("Denied.");
		}
	}
	
}
